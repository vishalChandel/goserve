package com.goserve.Activity

import android.app.Activity
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.app.Dialog
import android.app.TaskStackBuilder
import android.content.*
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.CalendarContract
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.goserve.R
import com.goserve.utils.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern
import android.widget.DatePicker
import android.widget.TextView
import java.io.ByteArrayOutputStream


open class BasicActivity : AppCompatActivity() {

    open var TAG: String = this@BasicActivity.javaClass.getSimpleName()
    open var mActivity: Activity = this@BasicActivity

    // - - Initialize other class objects
    var progressDialog: Dialog? = null

    // - - This is used to hide the status bar
    fun setStatusBar(mActivity: Activity?, mColor: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            window.statusBarColor = mColor
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
    }

    // - - To Show or Hide Password
    open fun setPasswordHideShow(editText: EditText, imageView: ImageView) {
        if (editText.transformationMethod.equals(PasswordTransformationMethod.getInstance())) {
            imageView.setImageResource(R.drawable.ic_eye_hide)
            //Show Password
            editText.transformationMethod = HideReturnsTransformationMethod.getInstance()
            editText.setSelection(editText.text.toString().trim().length)
        } else {
            imageView.setImageResource(R.drawable.ic_eye_show);
            //Hide Password
            editText.transformationMethod = PasswordTransformationMethod.getInstance()
            editText.setSelection(editText.text.toString().trim().length)
        }
    }

    // - - Date Picker
    open fun setDatePicker(mEditText: TextView) {
        // - - To Hide Keyboard
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mEditText.windowToken, 0)
        val mYear: Int
        val mMonth: Int
        val mDay: Int
        var mHour: Int
        var mMinute: Int
        val mDayOfWeek: Int
        // Get Current Date
        val c = Calendar.getInstance()
        //mEditText.setIs24HourView(true);
        mYear = c[Calendar.YEAR]
        mMonth = c[Calendar.MONTH]
        mDay = c[Calendar.DAY_OF_MONTH]
        mDayOfWeek = c[Calendar.DAY_OF_WEEK]
            val datePickerDialog = DatePickerDialog(
                mActivity,
                { view, year, monthOfYear, dayOfMonth ->
                    val intMonth = monthOfYear + 1
                    //String datee = Utilities.getFormatedString("" + dayOfMonth) + "-" + Utilities.getFormatedString("" + intMonth) + "-" + year;
                    val datee: String = getFormatedString("" + getFormatedString("" + intMonth)).toString() + "/" + dayOfMonth + "/" + year
                    mEditText.text = datee
                    mEditText.setTextColor(Color.BLACK)
                }, mYear, mMonth, mDay
            )
        var calendar3  = Calendar.getInstance()
        //Set Maximum date of calendar
        calendar3.add(Calendar.YEAR, -15);
            datePickerDialog.datePicker.maxDate = calendar3.timeInMillis
            datePickerDialog.show()

    }

    // - - Date Picker
    open fun datePicker(mTextView: TextView) {
        // - - To Hide Keyboard
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mTextView.windowToken, 0)
        val text: TextView? = null
        val sdf = SimpleDateFormat("MM/dd/yyyy", Locale.getDefault())
        val mycalender = Calendar.getInstance()
        val date = OnDateSetListener { view: DatePicker?, year: Int, month: Int, dayOfMonth: Int ->
            mycalender[Calendar.YEAR] = year
            mycalender[Calendar.MONTH] = month
            mycalender[Calendar.DAY_OF_MONTH] = dayOfMonth
            mTextView.setText(sdf.format(mycalender.time))
        }
        mTextView.setOnClickListener(View.OnClickListener { view: View? ->
            DatePickerDialog(
                this, date,
                mycalender[Calendar.YEAR],
                mycalender[Calendar.MONTH],
                mycalender[Calendar.DAY_OF_MONTH]
            ).show()
        })
        val currentDate = System.currentTimeMillis()
    }

    // - - To Show Alert Dialog
    open fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    // - - To Check Email Validation
    open fun isValidEmaillId(email: String?): Boolean {
        return Pattern.compile(
            "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                    + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                    + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                    + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                    + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$"
        ).matcher(email).matches()
    }

    // - - To switch between fragments*
    open fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager: FragmentManager = supportFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayout, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()

        }
    }


    // - - Edit text focus color change
    open fun editTextSelector(mEditText: View, rl: RelativeLayout, tag: String) {
        mEditText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                rl.setBackgroundResource(R.drawable.bg_login_outline_blue)
            } else {
                rl.setBackgroundResource(R.drawable.bg_outline_gray)
            }
        }

    }

    // - - To Check Whether User is logged_in or not
    fun isLogin(): Boolean {
        return AppPreference().readBoolean(mActivity, IS_LOGIN, false)
    }

    // - - To Get User Id
    fun userId(): String {
        return AppPreference().readString(mActivity, USERID, "")!!
    }

    // - - To Get User Role
    fun firstName(): String {
        return AppPreference().readString(mActivity, FIRST_NAME, "")!!
    }

    // - - To Get User Role
    fun lastName(): String {
        return AppPreference().readString(mActivity, LAST_NAME, "")!!
    }

    // - - To Get Access Token
    fun getAuthToken(): String {
        return AppPreference().readString(mActivity, AUTH_TOKEN, "")!!
    }

    // - - To Get UserName
    fun userName(): String {
        return AppPreference().readString(mActivity, USERNAME, "")!!
    }

    // - - To Get UserName
    fun dob(): String {
        return AppPreference().readString(mActivity, DOB, "")!!
    }

    // - - To Get OrgId
    fun orgId(): String {
        return AppPreference().readString(mActivity, ORG_ID, "")!!
    }

    // - - To Get Org Title
    fun orgTitle(): String {
        return AppPreference().readString(mActivity, ORG_TITLE, "")!!
    }

    // - - To Get Org Detail
    fun orgDetail(): String {
        return AppPreference().readString(mActivity, ORG_DETAIL, "")!!
    }

    // - - To Get Org Image
    fun orgImage(): String {
        return AppPreference().readString(mActivity, ORG_IMAGE, "")!!
    }

    // - - To Get User Email
    fun userEmail(): String {
        return AppPreference().readString(mActivity, EMAIL, "")!!
    }

    // - - To Get Profile Picture
    fun profilePicture(): String {
        return AppPreference().readString(mActivity, PROFILE_IMAGE, "")!!
    }

    // - - To Check Whether User has opted remember me or not
    fun isRememberMe(): Boolean {
        return RememberMeAppPreference().readBoolean(mActivity, IS_REMEMBER_ME, false)
    }

    // - - To Get Remember Me Email/UserName
    fun rememberMeEmailUserName(): String {
        return RememberMeAppPreference().readString(mActivity, RM_EMAIL_USERNAME, "")!!
    }

    // - - To Get Remember Me Password
    fun rememberMePassword(): String {
        return RememberMeAppPreference().readString(mActivity, RM_PASSWORD, "")!!
    }
    // - - To Get Filter Start Date
    fun starDate(): String {
        return mActivity?.let { AppPreference().readString(it, START_DATE, "") }!!
    }

    // - - To Get Filter End Date
    fun endDate(): String {
        return mActivity?.let { AppPreference().readString(it, END_DATE, "") }!!
    }

    // - - To Get Filter
    fun filter(): String {
        return mActivity?.let { AppPreference().readString(it, FILTER, "") }!!
    }

    // - - To Get SELECT Day
    fun selectDay(): String {
        return mActivity?.let { AppPreference().readString(it, SELECT_DAY, "") }!!
    }
    // - - To Start the New Activity
    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
    }

    // - - To Check Internet Connection
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }

    // - - To Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    // - - To Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)
            ?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    // - - To Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    // - - To Show Previous Activity Alert Dialog
    fun showPreviousActivityAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            startActivity(Intent(mActivity, LoginActivity::class.java))
            finish()
        }
        alertDialog.show()
    }

    // - - Authentication Failed Dialog
    fun showAuthFailedDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_auth_failed)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnLoginAgainB = alertDialog.findViewById<TextView>(R.id.btnLoginAgainB)
        btnLoginAgainB.setOnClickListener {
            alertDialog.dismiss()
            val preferences: SharedPreferences = AppPreference().getPreferences(applicationContext)
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            editor.commit()
            val mIntent = Intent(mActivity, LoginActivity::class.java)
            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent)
                .startActivities()
            // To clean up all activities
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mActivity?.startActivity(mIntent)
            mActivity?.finish()
        }
        alertDialog.show()
    }

    // - - To Get Formatted String
    open fun getFormatedString(strTemp: String): String? {
        var strActual = ""
        if (strTemp.length == 1) {
            strActual = "0$strTemp"
        } else if (strTemp.length == 2) {
            strActual = strTemp
        }
        return strActual
    }

    // - - Convert Bitmap to Byte Array
    open fun convertBitmapToByteArrayUncompressed(bitmap: Bitmap): ByteArray? {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 40, stream)
        return stream.toByteArray()
    }

    // - - To Get Alpha Numeric String
    open fun getAlphaNumericString(): String? {
        val n = 20

        // chose a Character random from this String
        val AlphaNumericString = ("ABCDEFGHIJKLMNOPQRSTUVWXYZ" /*+ "0123456789"*/
                + "abcdefghijklmnopqrstuvxyz")

        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0..n) {
            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length
                    * Math.random()).toInt()

            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString()
    }

    // - - Account Disabled Dialog
    fun showAccountDisabledDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_account_disabled)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnLoginAgainB = alertDialog.findViewById<TextView>(R.id.btnLoginAgainB)
        btnLoginAgainB.setOnClickListener {
            alertDialog.dismiss()
            val preferences: SharedPreferences = AppPreference().getPreferences(applicationContext)
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            editor.commit()
            val preferencesR: SharedPreferences = RememberMeAppPreference().getPreferences(applicationContext)
            val editorR = preferencesR.edit()
            editorR.clear()
            editorR.apply()
            editorR.commit()
            val mIntent = Intent(mActivity, LoginActivity::class.java)
            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent)
                .startActivities()
            // To clean up all activities
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mActivity?.startActivity(mIntent)
            mActivity?.finish()
        }
        alertDialog.show()
    }

    open fun addToCalendar(ctx: Context, title: String, dtstart: Long, dtend: Long) {
        val cr: ContentResolver = ctx.getContentResolver()
        val cursor: Cursor?
        cursor = if (Build.VERSION.SDK.toInt() >= 8) cr.query(
            Uri.parse("content://com.android.calendar/calendars"),
            arrayOf("_id", "calendar_displayName"),
            null,
            null,
            null
        ) else cr.query(
            Uri.parse("content://calendar/calendars"),
            arrayOf("_id", "calendar_displayName"),
            null,
            null,
            null
        )
        if (cursor!!.moveToFirst()) {
            val calNames = arrayOfNulls<String>(cursor.getCount())
            val calIds = IntArray(cursor.count)
            for (i in calNames.indices) {
                calIds[i] = cursor.getInt(0)
                calNames[i] = cursor.getString(1)
                cursor.moveToNext()
                Log.i("callNames",calNames.toString())
                Log.i("callIds",calIds.toString())
            }
            val builder: androidx.appcompat.app.AlertDialog.Builder = androidx.appcompat.app.AlertDialog.Builder(ctx)
            builder.setSingleChoiceItems(calNames, -1,
                DialogInterface.OnClickListener { dialog, which ->
                    val cv = ContentValues()
                    var callId=1
                    cv.put("calendar_id",1)
                    cv.put("title", title)
                    cv.put("dtstart", dtstart)
                    cv.put("hasAlarm", 1)
                    cv.put("dtend", dtend)
                    cv.put(CalendarContract.Events.EVENT_TIMEZONE, "Indian/Christmas");
                    Log.i("calender_id", calIds[which].toString())
                    val newEvent: Uri?
                    newEvent = if (Build.VERSION.SDK.toInt() >= 8) cr.insert(
                        Uri.parse("content://com.android.calendar/events"),
                        cv
                    ) else cr.insert(Uri.parse("content://calendar/events"), cv)
                    if (newEvent != null) {
                        val id: Long = newEvent.getLastPathSegment()!!.toLong()
                        val values = ContentValues()
                        values.put("event_id", id)
                        values.put("method", 1)
                        values.put("minutes", 15) // 15 minutes
                        if (Build.VERSION.SDK.toInt() >= 8) cr.insert(
                            Uri.parse("content://com.android.calendar/reminders"),
                            values
                        ) else cr.insert(Uri.parse("content://calendar/reminders"), values)
                    }
                    dialog.cancel()
                })
            builder.create().show()
        }
        cursor.close()
    }
}