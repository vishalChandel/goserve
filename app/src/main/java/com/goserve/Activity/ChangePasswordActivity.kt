package com.goserve.Activity

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import com.goserve.R
import com.goserve.databinding.ActivityChangePasswordBinding
import com.goserve.model.StatusMsgModel
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@ChangePasswordActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@ChangePasswordActivity

    var binding: ActivityChangePasswordBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)

        backgroundSelector()
        performClick()
    }


    fun backgroundSelector(){

        editTextSelector(binding?.oldPasswordET!!, binding!!.oldPasswordRL,"")
        editTextSelector(binding!!.newPasswordET, binding!!.newPasswordRL,"")
        editTextSelector(binding!!.confirmPasswordET, binding!!.confirmPasswordRL,"")
    }

    fun performClick() {
        binding!!.imgArrowRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        binding!!.btSubmitTV.setOnClickListener(View.OnClickListener {
            if (isValidate()) {
                if (isNetworkAvailable(mActivity)) {
                    executeChangePwdApi()
                } else
                    showToast(mActivity, getString(R.string.internet_connection_error))
            }

        })
        binding?.changePasswordEyeRL?.setOnClickListener(View.OnClickListener {
            setPasswordHideShow(binding!!.oldPasswordET, binding?.changePasswordEyeIV!!)
        })
        binding?.newPasswordEyeRL?.setOnClickListener(View.OnClickListener {
            setPasswordHideShow(binding!!.newPasswordET, binding?.newPasswordEyeIV!!)
        })
        binding?.confirmPasswordEyeRL?.setOnClickListener(View.OnClickListener {
            setPasswordHideShow(binding!!.confirmPasswordET, binding?.confirmPasswordEyeIV!!)
        })

    }

    private fun mChangePwdParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = getAuthToken()
        mMap["oldPassword"] = binding!!.oldPasswordET.text.toString()
        mMap["newPassword"] = binding!!.newPasswordET.text.toString()
        mMap["confirmPassword"] = binding!!.confirmPasswordET.text.toString()
        Log.e("ChangePwdParams:", "$mMap")
        return mMap
    }

    private fun executeChangePwdApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface = RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.changePwdRequest(mChangePwdParams()).enqueue(object : Callback<StatusMsgModel> {
            override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<StatusMsgModel>, response: Response<StatusMsgModel>) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        showToast(mActivity,response.body()?.message)
                        binding!!.oldPasswordET.setText("")
                        binding!!.newPasswordET.setText("")
                        binding!!.confirmPasswordET.setText("")
                        onBackPressed()
                    }
                    400 -> {
                        showAlertDialog(mActivity,getString(R.string.current_password_does_not_match))
                        binding!!.oldPasswordET.setText("")
                        binding!!.newPasswordET.setText("")
                        binding!!.confirmPasswordET.setText("")
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    private fun isValidate(): Boolean {
        var flag = true
        if (binding?.oldPasswordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_old))
            flag = false
        } else if (binding?.newPasswordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_new))
            flag = false
        } else if (binding?.newPasswordET?.getText().toString().trim { it <= ' ' }.length < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit))
            flag = false
        } else if (binding?.confirmPasswordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_confirm))
            flag = false
        } else if (!binding?.newPasswordET?.getText().toString().equals(binding?.confirmPasswordET?.getText().toString())) {
            showAlertDialog(mActivity, getString(R.string.match_password))
            flag = false
        }
        return flag
    }

}