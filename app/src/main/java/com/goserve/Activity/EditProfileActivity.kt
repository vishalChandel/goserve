package com.goserve.Activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.*
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.aigestudio.wheelpicker.WheelPicker
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.goserve.R
import com.goserve.databinding.ActivityEditProfileBinding
import com.goserve.model.GetAllOrganisationModel
import com.goserve.model.OrganisationDataItem
import com.goserve.model.StatusMsgModel
import com.goserve.model.UserDataModel
import com.goserve.utils.*
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.bottom_sheet_camera_gallery.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response
import java.io.IOException
import java.util.*

class EditProfileActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@EditProfileActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@EditProfileActivity

    var binding: ActivityEditProfileBinding? = null
    private var mOrgName: String = ""
    private var mOrgId: String = ""
    private var mBitmap: Bitmap? = null
    var mOrganisationList: ArrayList<OrganisationDataItem?>? = ArrayList<OrganisationDataItem?>()

    // - - Initialize Manifest Permission : Camera and Gallery
    private var writeExternalStorage = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    private var writeReadStorage = android.Manifest.permission.READ_EXTERNAL_STORAGE
    private var writeCamera = android.Manifest.permission.CAMERA


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEditProfileBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        setStatusBar(mActivity, Color.WHITE)
        setDataOnWidgets()
        executeOrganisationApi()
        performClick()
        backgroundSelector()
        textTouchSelector()
    }

    private fun setDataOnWidgets() {
        binding!!.firstNameET.setText(firstName())
        binding!!.lastNameET.setText(lastName())
        binding!!.chooseET.setText(userName())
        binding!!.emailET.setText(userEmail())
        binding!!.yourBirthTV.text = dob()
        Glide
            .with(this)
            .load(profilePicture())
            .centerCrop()
            .placeholder(R.drawable.ic_placeholder)
            .into(binding!!.imgEditProfile)
        binding!!.spinnerTV.text = orgTitle()

    }

    // your birthday set bg blue
    private fun textTouchSelector() {

        binding!!.spinnerTV.setOnTouchListener(View.OnTouchListener { v, event ->
            createRelationshipDialog(mActivity)
            false
        })
    }


    // your birthday set bg gray
    override fun editTextSelector(mEditText: View, rl: RelativeLayout, tag: String) {
        mEditText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            binding!!.yourBirthRL.setBackgroundResource(R.drawable.bg_outline_gray)
            binding!!.organizationRL.setBackgroundResource(R.drawable.bg_outline_gray)
            if (hasFocus) {
                rl.setBackgroundResource((R.drawable.bg_login_outline_blue))
            } else {
                rl.setBackgroundResource(R.drawable.bg_outline_gray)
            }
        }
    }

    fun backgroundSelector() {
        editTextSelector(binding?.firstNameET!!, binding!!.firstNameRL, "")
        editTextSelector(binding!!.lastNameET, binding!!.lastNameRL, "")
        editTextSelector(binding!!.chooseET, binding!!.chooseRL, "")
    }

    fun performClick() {
        binding?.btSaveTV?.setOnClickListener(View.OnClickListener {

            if (isValidate()) {
                executeEditProfileApi()
            }
        })

        binding!!.yourBirthRL.setOnClickListener {
            setDatePicker(binding!!.yourBirthTV)
        }

        binding!!.imgArrowRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        binding?.imgEditProfile?.setOnClickListener(View.OnClickListener {
            openCameraGalleryBottomSheet()
        })

        binding?.imgCamreaIV?.setOnClickListener(View.OnClickListener {
            openCameraGalleryBottomSheet()
        })
    }

    // appply validation
    fun isValidate(): Boolean {
        var flag = true
        if (binding?.firstNameET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_name))
            flag = false
        } else if (binding?.lastNameET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (binding?.chooseET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_choose_username))
            flag = false
        } else if (binding?.emailET?.getText().toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.getText().toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (binding?.yourBirthTV?.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_birth_date))
            flag = false
        }
        return flag
    }

    // - - To Check permissions
    private fun checkPermission(): Boolean {
        val write = ContextCompat.checkSelfPermission(mActivity, writeExternalStorage)
        val read = ContextCompat.checkSelfPermission(mActivity, writeReadStorage)
        val camera = ContextCompat.checkSelfPermission(mActivity, writeCamera)
        return write == PackageManager.PERMISSION_GRANTED && read == PackageManager.PERMISSION_GRANTED && camera == PackageManager.PERMISSION_GRANTED
    }

    // - - To Request permissions
    private fun requestPermission() {
        ActivityCompat.requestPermissions(
            mActivity, arrayOf(writeExternalStorage, writeReadStorage, writeCamera), 369
        )
    }

    // - - After Request permissions
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            369 -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                } else {
                    //onSelectImageClick()
                    Log.e(TAG, "Permission has been granted by user")
                }
            }
        }
    }

    // - - Start Image Picker Activity
    private fun onSelectImageClick() {
        ImagePicker.with(this)
            //Crop image(Optional), Check Customization for more option
            .crop()
            .galleryOnly()

            //Final image size will be less than 1 MB(Optional)
            .compress(200)

            //Final image resolution will be less than 1080 x 1080(Optional)
            .maxResultSize(512, 512)
            .cropSquare()
            .start()
    }

    // - - Start Image Picker Activity
    private fun onSelectCameraClick() {
        ImagePicker.with(this)
            //Crop image(Optional), Check Customization for more option
            .crop()
            .cameraOnly()

            //Final image size will be less than 1 MB(Optional)
            .compress(200)

            //Final image resolution will be less than 1080 x 1080(Optional)
            .maxResultSize(512, 512)
            .cropSquare()
            .start()
    }

    // - - SetUp Image from Camera/Gallery on View
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (resultCode) {
            RESULT_OK -> {
                try {
                    //Image Uri will not be null for RESULT_OK
                    val uri: Uri = data?.data!!

                    // Use Uri object instead of File to avoid storage permissions
                    Glide
                        .with(this)
                        .load(uri)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .into(binding!!.imgEditProfile)

                    val imageStream = contentResolver.openInputStream(uri)
                    val selectedImage = BitmapFactory.decodeStream(imageStream)
                    mBitmap = selectedImage
                } catch (e: IOException) {
                    Log.e(TAG, "****Error****" + e.printStackTrace())
                    e.printStackTrace()
                }

            }
            ImagePicker.RESULT_ERROR -> {
                Toast.makeText(this, ImagePicker.getError(data), Toast.LENGTH_SHORT).show()
            }
        }
    }


    fun openCameraGalleryBottomSheet() {

        val dialog = BottomSheetDialog(this)
        val view: View =
            LayoutInflater.from(this).inflate(R.layout.bottom_sheet_camera_gallery, null)
        dialog!!.setContentView(view)

        (view.parent as View).setBackgroundColor(Color.TRANSPARENT)
        (view.parent as View).backgroundTintMode = PorterDuff.Mode.CLEAR
        (view.parent as View).backgroundTintList = ColorStateList.valueOf(Color.TRANSPARENT)

        var choosePhotoTv = view.findViewById<TextView>(R.id.choosePhotoTv)
        var takePhotoTV = view.findViewById<TextView>(R.id.takePhotoTV)
        var btCancelTV = view.findViewById<TextView>(R.id.btCancelTV)
        var deleteTV = view.findViewById<TextView>(R.id.deleteTV)
        var view1 = view.findViewById<View>(R.id.view1)
        if (profilePicture().isNullOrEmpty()) {
            deleteTV.visibility = View.GONE
            view1.visibility = View.GONE
        }
        choosePhotoTv?.setOnClickListener(View.OnClickListener {
            if (checkPermission()) {
                onSelectImageClick()
                dialog.dismiss()
            } else {
                requestPermission()
            }
        })

        takePhotoTV?.setOnClickListener(View.OnClickListener {
            if (checkPermission()) {
                onSelectCameraClick()
                dialog.dismiss()
            } else {
                requestPermission()
            }
        })

        btCancelTV.setOnClickListener(View.OnClickListener {
            dialog.dismiss()
        })

        deleteTV.setOnClickListener(View.OnClickListener {
            executeDeleteProfileImageApi()
            dialog.dismiss()
        })

        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()

    }

    // - - Execute Signup Api
    private fun executeEditProfileApi() {
        val accessToken: RequestBody =
            getAuthToken().toString().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val userName: RequestBody = binding!!.chooseET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val firstName: RequestBody = binding!!.firstNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val lastName: RequestBody = binding!!.lastNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val orgId: RequestBody = mOrgId.trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val dob: RequestBody = binding!!.yourBirthTV.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        var profileImage: okhttp3.MultipartBody.Part? = null
        if (mBitmap != null) {
            val requestFile1: okhttp3.RequestBody? =
                convertBitmapToByteArrayUncompressed(mBitmap!!)?.let {
                    okhttp3.RequestBody.create(
                        "multipart/form-data".toMediaTypeOrNull(),
                        it
                    )
                }
            profileImage = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "profileImage",
                    getAlphaNumericString()!!.toString() + ".jpeg",
                    it
                )

            }
        }
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.editProfileRequest(
            profileImage,
            accessToken,
            userName,
            firstName,
            lastName,
            orgId,
            dob
        ).enqueue(object : Callback<UserDataModel> {
            override fun onFailure(call: Call<UserDataModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<UserDataModel>,
                response: Response<UserDataModel>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        showToast(mActivity, getString(R.string.profile_updated_successfully))
                        AppPreference().writeString(
                            mActivity,
                            FIRST_NAME,
                            response.body()?.data?.firstName
                        )
                        AppPreference().writeString(
                            mActivity,
                            LAST_NAME,
                            response.body()?.data?.lastName
                        )
                        AppPreference().writeString(
                            mActivity,
                            USERNAME,
                            response.body()?.data?.userName
                        )
                        AppPreference().writeString(
                            mActivity,
                            PROFILE_IMAGE,
                            response.body()?.data?.profileImage
                        )
                        AppPreference().writeString(mActivity, DOB, response.body()?.data?.dob)
                        AppPreference().writeString(
                            mActivity,
                            ORG_ID,
                            response.body()?.data?.orgID
                        )
                        AppPreference().writeString(
                            mActivity,
                            HOUR_TRACKER,
                            response.body()?.data?.hourTracker
                        )
                        setDataOnWidgets()
                        finish()
                    }
                    400 -> {
                        val converter: Converter<ResponseBody, UserDataModel> =
                            RetrofitClient.retrofit?.responseBodyConverter(
                                UserDataModel::class.java, arrayOfNulls<Annotation>(0)
                            )!!
                        val error: UserDataModel
                        try {
                            error = converter.convert(response.errorBody())!!
                            Log.e("error message", error.message!!)
                            showAlertDialog(mActivity, error.message)
                        } catch (e: IOException) {
                            e.printStackTrace()
                        }
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    private fun mOrganisationApiParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["search"] = ""
        Log.e("OrganisationApiParams:", "$mMap")
        return mMap
    }

    // - - Execute Organisation Api
    private fun executeOrganisationApi() {
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.getAllOrganisationForStudentRequest(mOrganisationApiParams())
            .enqueue(object : Callback<GetAllOrganisationModel> {
                override fun onResponse(
                    call: Call<GetAllOrganisationModel>,
                    response: Response<GetAllOrganisationModel>
                ) {
                    when (response.code()) {
                        200 -> {
                            mOrganisationList = response.body()?.data as ArrayList<OrganisationDataItem?>?
                        }
                        401 -> {
                            showAuthFailedDialog(mActivity)
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

                override fun onFailure(call: Call<GetAllOrganisationModel>, t: Throwable) {
                }
            })
    }

    fun createRelationshipDialog(mActivity: Activity) {
        val mDataList: ArrayList<String> = ArrayList<String>()
        for (i in 0 until mOrganisationList?.size!!) {
            mDataList.add(mOrganisationList?.get(i)?.orgTitle!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_org, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val relationshipWP: WheelPicker = sheetView.findViewById(R.id.relationshipWP)
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        relationshipWP.setAtmospheric(true)
        relationshipWP.isCyclic = false
        relationshipWP.isCurved = true
        //Set Data
        relationshipWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }
        txtSaveTV.setOnClickListener {
            mOrgId = "" + mOrganisationList!![relationshipWP.currentItemPosition]?.orgID
            mOrgName = mDataList.get(relationshipWP.currentItemPosition)
            binding!!.spinnerTV.text = mOrgName
            mBottomSheetDialog.dismiss()
        }
    }

    private fun mDeleteProfileImageParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = getAuthToken()
        return mMap
    }

    // - - Execute Disease Api
    private fun executeDeleteProfileImageApi() {
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.deleteProfileImageRequest(mDeleteProfileImageParams())
            .enqueue(object : Callback<StatusMsgModel> {
                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    when (response.code()) {
                        200 -> {
                            AppPreference().writeString(mActivity, PROFILE_IMAGE, "")
                            Glide
                                .with(mActivity)
                                .load(profilePicture())
                                .centerCrop()
                                .placeholder(R.drawable.ic_placeholder)
                                .into(binding!!.imgEditProfile)
                            showToast(mActivity, response.body()?.message)
                        }
                        401 -> {
                            showAuthFailedDialog(mActivity)
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                }
            })
    }

}