package com.goserve.Activity

import android.annotation.SuppressLint
import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.content.res.AppCompatResources
import com.goserve.databinding.ActivityFilterBinding
import kotlinx.android.synthetic.main.activity_filter.*
import com.goserve.R
import com.goserve.utils.*

class FilterActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@FilterActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@FilterActivity
    var binding: ActivityFilterBinding? = null
    var mList: ArrayList<String> = ArrayList()
    var selectDay=""

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFilterBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)
        performClick()
        textTouchSelector()
        AppPreference().writeString(mActivity!!, START_DATE, "")
        AppPreference().writeString(mActivity!!, END_DATE, "")
        AppPreference().writeString(mActivity!!, FILTER, "")
        AppPreference().writeString(mActivity!!, SELECT_DAY, "")
    }


    @RequiresApi(Build.VERSION_CODES.M)
    @SuppressLint("ResourceAsColor")
    fun performClick() {
        binding!!.MonTV.setOnClickListener(View.OnClickListener {
            setWeekDays(mList, "'Mon'", binding!!.MonTV)
        })

        binding!!.TueTV.setOnClickListener(View.OnClickListener {
            setWeekDays(mList, "'Tue'", binding!!.TueTV)
        })

        binding!!.WedTV.setOnClickListener(View.OnClickListener {
            setWeekDays(mList, "'Wed'", binding!!.WedTV)
        })

        binding!!.ThuTV.setOnClickListener(View.OnClickListener {
            setWeekDays(mList, "'Thu'", binding!!.ThuTV)
        })

        binding!!.FriTV.setOnClickListener(View.OnClickListener {
            setWeekDays(mList, "'Fri'", binding!!.FriTV)

        })

        binding!!.SatTV.setOnClickListener(View.OnClickListener {
            setWeekDays(mList, "'Sat'", binding!!.SatTV)
        })

        binding!!.SunTV.setOnClickListener(View.OnClickListener {
            setWeekDays(mList, "'Sun'", binding!!.SunTV)
        })

        binding!!.imgArrowRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        binding!!.startDateTV.setOnClickListener(View.OnClickListener {
            datePicker(binding?.startDateTV!!)
        })

        binding!!.endDateTV.setOnClickListener(View.OnClickListener {
            datePicker(binding?.endDateTV!!)
        })


        binding!!.btLoginTV.setOnClickListener(View.OnClickListener {
            if (isValidate()) {
                if(!mList.isNullOrEmpty()){
                    selectDay = mList.joinToString { it }
                }

                AppPreference().writeString(mActivity!!, START_DATE, binding!!.startDateTV.text.toString())
                AppPreference().writeString(mActivity!!, END_DATE, binding!!.endDateTV.text.toString())
                AppPreference().writeString(mActivity!!, FILTER, "filter")
                AppPreference().writeString(mActivity!!, SELECT_DAY, selectDay)
                onBackPressed()
            }
        })

    }

    @SuppressLint("ResourceAsColor")
    @RequiresApi(Build.VERSION_CODES.M)
    fun setWeekDays(mList: ArrayList<String>, type: String, textView: TextView) {
        if (mList.contains(type)) {
            mList.remove(type)
            textView.setBackgroundResource(R.drawable.bg_tabs_bt)
            textView.setTextColor(resources.getColor(R.color.black))
        } else {
            mList.add(type)
            textView.setBackgroundResource(R.drawable.bg_filter_bt_blue)
            textView.setTextColor(AppCompatResources.getColorStateList(this, R.color.white))
        }

    }

    // background set blue when  on focus
    private fun textTouchSelector() {

        binding!!.startDateTV.setOnTouchListener(View.OnTouchListener { v, event ->
            binding?.startDateTV?.requestFocus()
            datePicker(binding?.startDateTV!!)
            binding!!.startDateRL.setBackgroundResource(R.drawable.bg_login_outline_blue)
            binding!!.endDateRL.setBackgroundResource(R.drawable.bg_outline_gray)
            false
        })

        binding!!.endDateTV.setOnTouchListener(View.OnTouchListener { v, event ->
            binding?.endDateTV?.requestFocus()
            datePicker(binding?.endDateTV!!)
            binding!!.endDateRL.setBackgroundResource(R.drawable.bg_login_outline_blue)
            binding!!.startDateRL.setBackgroundResource(R.drawable.bg_outline_gray)
            false
        })
    }


    // background set gray when not on focus
    override fun editTextSelector(mEditText: View, rl: RelativeLayout, tag: String) {
        mEditText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            binding!!.startDateRL.setBackgroundResource(R.drawable.bg_outline_gray)
            if (hasFocus) {
                rl.setBackgroundResource(R.drawable.bg_login_outline_blue)
            } else {
                rl.setBackgroundResource(R.drawable.bg_outline_gray)
            }
        }
    }

    private fun isValidate(): Boolean {
        var flag = true
        if (binding?.startDateTV?.getText().toString().trim { it <= ' ' } == "" && binding?.endDateTV?.getText().toString().trim { it <= ' ' } == "" && mList.isNullOrEmpty() ) {
            showAlertDialog(mActivity, getString(R.string.please_choose_filter))
            flag = false
        }
        return flag
    }

    override fun onResume() {
        super.onResume()
    }
}