package com.goserve.Activity

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import com.goserve.R
import com.goserve.databinding.ActivityForgotPasswordBinding
import com.goserve.model.StatusMsgModel
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@ForgotPasswordActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@ForgotPasswordActivity

    private lateinit var binding: ActivityForgotPasswordBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgotPasswordBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.WHITE)
        performClick()
        backgroundSelector()
    }

    fun performClick() {
        binding.btsubmitTV.setOnClickListener(View.OnClickListener {
            if (isValidate()) {
                if (isNetworkAvailable(mActivity)) {
                    executeForgotPasswordApi()
                } else
                    showToast(mActivity, getString(R.string.internet_connection_error))
            }

        })
    }

    fun backgroundSelector() {
        editTextSelector(binding.emailET, binding.emailRL, "")
    }

    private fun mForgotPwdParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["email"] = binding!!.emailET.text.trim().toString()
        Log.e("ForgotPwdParams:", "$mMap")
        return mMap
    }

    private fun executeForgotPasswordApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.forgotPwdRequest(mForgotPwdParams())
            .enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                    showToast(mActivity, "Failure")
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            showPreviousActivityAlertDialog(mActivity, getString(R.string.new_password_has_been_sent))
                            binding!!.emailET.setText("")
                        }
                        400 -> {
                            showAlertDialog(mActivity,getString((R.string.this_email_does_not_exist)))
                            binding!!.emailET.setText("")
                        }
                        406 ->{
                            showAlertDialog(mActivity, getString(R.string.email_not_verified))
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    private fun isValidate(): Boolean {
        var flag = true
        if (binding?.emailET?.getText().toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.getText().toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        }
        return flag
    }
}