package com.goserve.Activity.Fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.viewpager.widget.ViewPager
import com.bumptech.glide.Glide
import com.google.android.material.tabs.TabLayout
import com.google.android.material.tabs.TabLayout.OnTabSelectedListener
import com.goserve.Activity.EditProfileActivity
import com.goserve.Activity.OrganisationDetailActivity
import com.goserve.Activity.SettingActivity
import com.goserve.Adapter.Pager_Adapter
import com.goserve.R
import com.goserve.model.*
import com.goserve.utils.*
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import kotlinx.android.synthetic.main.fragment_account.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.MutableMap
import kotlin.collections.set


class AccountFragment : BasicFragment() {
    // - - Getting the Current Class Name
    override var TAG = this@AccountFragment.javaClass.simpleName

    // - - Initialize Objects
    private var tabLayout: TabLayout? = null
    private var viewPager: ViewPager? = null
    private var adapter: Pager_Adapter? = null
    private var imgSearchRL: RelativeLayout? = null
    private var profileIV: ImageView? = null
    private var schoolTV: TextView? = null
    private val tabIcons = intArrayOf(R.drawable.ic_up_png, R.drawable.ic_past_gray)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_account, container, false)

        // find view id
        imgSearchRL = view.findViewById<View>(R.id.imgSearchRL) as RelativeLayout
        profileIV = view.findViewById<View>(R.id.profileIV) as ImageView
        tabLayout = view.findViewById<View>(R.id.tabLayout) as TabLayout
        schoolTV = view.findViewById<View>(R.id.schoolTV) as TextView

        tabLayout!!.addTab(tabLayout!!.newTab().setIcon(R.drawable.ic_up_black))
        tabLayout!!.addTab(tabLayout!!.newTab().setIcon(R.drawable.ic_past_black))

        viewPager = view.findViewById<View>(R.id.viewPager) as ViewPager
        adapter = Pager_Adapter(childFragmentManager)
        tabLayout!!.setupWithViewPager(viewPager)
        setupViewPager(viewPager!!)
        setupTabIcons()
        performClick()
        return view
    }

    private fun setupTabIcons() {
        tabLayout?.getTabAt(0)?.setIcon(tabIcons[0])
        tabLayout?.getTabAt(1)?.setIcon(tabIcons[1])
        tabLayout!!.addOnTabSelectedListener(object : OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                tabLayout?.getTabAt(0)?.setIcon(R.drawable.ic_up_png)
                tabLayout?.getTabAt(1)?.setIcon(R.drawable.ic_past_black)

            }

            override fun onTabReselected(tab: TabLayout.Tab) {


            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
        })
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = Pager_Adapter(childFragmentManager)
        adapter.addFrag(UpFragment(), "")
        adapter.addFrag(OpporFragment(), "")
        viewPager.adapter = adapter
    }

    fun performClick() {
        schoolTV!!.setOnClickListener {
            val intent = Intent(context, OrganisationDetailActivity::class.java)
            startActivity(intent)
        }

        imgSearchRL?.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, SettingActivity::class.java)
            startActivity(intent)
        })

        profileIV?.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, EditProfileActivity::class.java)
            startActivity(intent)
        })
    }

    private fun mGetProfileParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = authToken()
        return mMap
    }

    private fun executeGetProfileApi() {
        showProgressDialog(activity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.getProfileRequest(mGetProfileParams())
            .enqueue(object : Callback<GetProfileModel> {
                override fun onFailure(call: Call<GetProfileModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetProfileModel>,
                    response: Response<GetProfileModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            if (activity != null) {
                                AppPreference().writeString(
                                    activity!!,
                                    FIRST_NAME,
                                    response.body()?.data?.firstName
                                )
                                AppPreference().writeString(
                                    activity!!,
                                    LAST_NAME,
                                    response.body()?.data?.lastName
                                )
                                AppPreference().writeString(
                                    activity!!,
                                    USERNAME,
                                    response.body()?.data?.userName
                                )
                                AppPreference().writeString(
                                    activity!!,
                                    PROFILE_IMAGE,
                                    response.body()?.data?.profileImage
                                )
                                AppPreference().writeString(activity!!, ORG_TITLE, response.body()?.data?.organizationDetails?.orgTitle)
                                AppPreference().writeString(activity!!, ORG_DETAIL, response.body()?.data?.organizationDetails?.orgDescription)
                                AppPreference().writeString(activity!!, ORG_IMAGE, response.body()?.data?.organizationDetails?.orgImage)
                                AppPreference().writeString(activity!!, DOB, response.body()?.data?.dob
                                )
                                AppPreference().writeString(
                                    activity!!,
                                    ORG_ID,
                                    response.body()?.data?.orgID
                                )
                                AppPreference().writeString(
                                    activity!!,
                                    HOUR_TRACKER,
                                    response.body()?.data?.hourTracker
                                )
                                nameTV.text =
                                    response.body()?.data?.firstName + " " + response.body()?.data?.lastName
                                emailTV.text = response.body()?.data?.email
                                activity?.let {
                                    Glide.with(it).load(response.body()?.data?.profileImage)
                                        .placeholder(R.drawable.ic_placeholder)
                                        .error(R.drawable.ic_placeholder)
                                        .into(profileIV!!)
                                }
                                schoolTV?.text =
                                    response.body()?.data?.organizationDetails?.orgTitle
                                hourTrackerTV.text =
                                    "Hour Tracker: ${response.body()?.data?.hourTracker}"
                            }
                        }
                        403 -> {
                            showAccountDisabledDialog(activity)
                        }
                        401 -> {
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    fun setDataOnWidgets() {
        nameTV.text = firstName() + " " + lastName()
        emailTV.text = userEmail()
        activity?.let {
            Glide.with(it).load(profilePicture())
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .into(profileIV!!)
        }
        schoolTV?.text = orgTitle()
        hourTrackerTV.text = "Hour Tracker: ${hourTracker()}"
    }

    override fun onResume() {
        super.onResume()
        executeGetProfileApi()
        setDataOnWidgets()
    }
}