package com.goserve.Activity.Fragment

import android.app.Activity
import android.app.Dialog
import android.app.TaskStackBuilder
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.goserve.Activity.WelcomeActivity
import com.goserve.R
import com.goserve.utils.*
import java.util.*

open class BasicFragment : Fragment() {
    // - - Get Class Name
    open var TAG = this@BasicFragment.javaClass.simpleName

    // - - Initialize other class objects
    var progressDialog: Dialog? = null

    // - - To switch between fragments*
    open fun switchFragment(fragment: Fragment?, Tag: String?, addToStack: Boolean, bundle: Bundle?) {
        val fragmentManager: FragmentManager = childFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayout, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()

        }
    }

    // - - To Get First Name
    fun firstName(): String {
        return activity?.let { AppPreference().readString(it, FIRST_NAME, "") }!!
    }

    // - - To Get Last Name
    fun lastName(): String {
        return activity?.let { AppPreference().readString(it, LAST_NAME, "") }!!
    }

    // - - To Get OrgId
    fun orgId(): String {
        return activity?.let { AppPreference().readString(it, ORG_ID, "") }!!
    }

    // - - To Get Org Title
    fun orgTitle(): String {
        return activity?.let { AppPreference().readString(it, ORG_TITLE, "") }!!
    }

    // - - To Get Org Detail
    fun orgDetail(): String {
        return activity?.let { AppPreference().readString(it, ORG_DETAIL, "") }!!
    }

    // - - To Get Org Image
    fun orgImage(): String {
        return activity?.let { AppPreference().readString(it, ORG_IMAGE, "") }!!
    }

    // - - To Get User Email
    fun userEmail(): String {
        return activity?.let { AppPreference().readString(it, EMAIL, "") }!!
    }

    // - - To Get AuthToken
    fun authToken(): String {
        return activity?.let { AppPreference().readString(it, AUTH_TOKEN, "") }!!
    }

    // - - To Get UserName
    fun userName(): String {
        return activity?.let { AppPreference().readString(it, USERNAME, "") }!!
    }

    // - - To Get DOB
    fun dob(): String {
        return activity?.let { AppPreference().readString(it, DOB, "") }!!
    }

    // - - To Get Profile Picture
    fun profilePicture(): String {
        return activity?.let { AppPreference().readString(it, PROFILE_IMAGE, "") }!!
    }

    // - - To Get Hours
    fun hourTracker(): String {
        return activity?.let { AppPreference().readString(it, HOUR_TRACKER, "") }!!
    }

    // - - To Get Filter Start Date
    fun starDate(): String {
        return activity?.let { AppPreference().readString(it, START_DATE, "") }!!
    }

    // - - To Get Filter End Date
    fun endDate(): String {
        return activity?.let { AppPreference().readString(it, END_DATE, "") }!!
    }

    // - - To Get Filter
    fun filter(): String {
        return activity?.let { AppPreference().readString(it, FILTER, "") }!!
    }

    // - - To Get SELECT Day
    fun selectDay(): String {
        return activity?.let { AppPreference().readString(it, SELECT_DAY, "") }!!
    }

    // - - To Get Search Text
    fun searchText(): String {
        return activity?.let { AppPreference().readString(it, SEARCH_TEXT, "") }!!
    }

    // - - To Show Toast Message
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }

    // - - To Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

    // - - To Show Progress Dialog
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }

    // - - To Hide Progress Dialog
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    // - - Authentication Failed Dialog
    fun showAuthFailedDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_auth_failed)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnLoginAgainB = alertDialog.findViewById<TextView>(R.id.btnLoginAgainB)
        btnLoginAgainB.setOnClickListener {
            alertDialog.dismiss()
            val preferences: SharedPreferences = mActivity?.let { AppPreference().getPreferences(it)}!!
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            editor.commit()
            val mIntent = Intent(mActivity, WelcomeActivity::class.java)
            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
            // To clean up all activities
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mActivity?.startActivity(mIntent)
            mActivity?.finish()
        }
        alertDialog.show()
    }

    // - - Account Disabled Dialog
    fun showAccountDisabledDialog(mActivity: Activity?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_account_disabled)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        val btnLoginAgainB = alertDialog.findViewById<TextView>(R.id.btnLoginAgainB)
        btnLoginAgainB.setOnClickListener {
            alertDialog.dismiss()
            val preferences: SharedPreferences = mActivity?.let { AppPreference().getPreferences(it)}!!
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            editor.commit()
            val mIntent = Intent(mActivity, WelcomeActivity::class.java)
            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent).startActivities()
            // To clean up all activities
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            mActivity?.startActivity(mIntent)
            mActivity?.finish()
        }
        alertDialog.show()
    }
}