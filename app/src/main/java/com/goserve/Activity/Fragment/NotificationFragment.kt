package com.goserve.Activity.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goserve.Adapter.NotificationAdapter
import com.goserve.R
import com.goserve.model.NotificationDataItem
import com.goserve.model.NotificationModel
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import kotlinx.android.synthetic.main.fragment_notification.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class NotificationFragment : BasicFragment(){
    // - - Getting the Current Class Name
    override var TAG = this@NotificationFragment.javaClass.simpleName
    // - - Initialize Objects
    var mNotificationList: ArrayList<NotificationDataItem?>? = ArrayList<NotificationDataItem?>()
    var notificationRV: RecyclerView? = null
    var notificationAdapter: NotificationAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.fragment_notification, container, false)
        notificationRV = view.findViewById(R.id.notificationRV)
        executeNotificationApi()
        return view
    }

    private fun mNotificationParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = authToken()
        mMap["pageNo"] ="1"
        Log.e("NotificationParams:", "$mMap")
        return mMap
    }

    private fun executeNotificationApi() {
        showProgressDialog(activity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.notificationRequest(mNotificationParams())
            .enqueue(object : Callback<NotificationModel> {
                override fun onFailure(call: Call<NotificationModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<NotificationModel>,
                    response: Response<NotificationModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            if(activity!=null){
                            mNotificationList = response.body()?.data as ArrayList<NotificationDataItem?>?
                            if(mNotificationList!!.isNullOrEmpty() || mNotificationList!!.size==0){
                                txtNoDataTV.visibility=View.VISIBLE
                            }else {
                                txtNoDataTV.visibility = View.GONE
                                setAdapterRV()
                            }}
                        }
                        403 -> {
                            showAccountDisabledDialog(activity)
                        }
                        400 -> {
                            if(activity!=null) {
                                txtNoDataTV.visibility = View.VISIBLE
                            }
                        }
                        401 ->{
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    fun setAdapterRV() {
        var layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        notificationRV?.layoutManager = layoutManager
        notificationAdapter = NotificationAdapter(activity,mNotificationList)
        notificationRV?.adapter = notificationAdapter
    }
}