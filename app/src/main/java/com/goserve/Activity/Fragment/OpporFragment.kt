package com.goserve.Activity.Fragment

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.goserve.Adapter.UpAdapter
import com.goserve.Interfaces.InterfaceBottomSheet
import com.goserve.Interfaces.LoadMorePastOp
import com.goserve.R
import com.goserve.model.GetAllOpportunitiesByTypeModel
import com.goserve.model.OpportunitiesDataItem
import com.goserve.model.PastOpByTypeModel
import com.goserve.model.PastOpDataItem
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.fragment_oppor.*
import kotlinx.android.synthetic.main.fragment_oppor.txtNoDataTV
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class OpporFragment : BasicFragment(), LoadMorePastOp {
    // - - Getting the Current Class Name
    override var TAG = this@OpporFragment.javaClass.simpleName
    // - - Initialize Objects
    var mPastOpList: ArrayList<OpportunitiesDataItem?>? = ArrayList<OpportunitiesDataItem?>()
    var mPageNo: Int = 1
    var mLoadMore: LoadMorePastOp? = null
    var isLoading: Boolean = true
    var opportuntiesRV: RecyclerView? = null
    var mPastOpAdapter: UpAdapter? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_oppor, container, false)
        opportuntiesRV = view.findViewById(R.id.opportuntiesRV)
        mLoadMore = this
        if (mPastOpList != null) {
            mPastOpList!!.clear()
        }
        executePastOpByTypeApi()
        return view
    }

    private fun mPastOpParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = authToken()
        mMap["type"] = "2"
        mMap["perPage"] = "20"
        mMap["pageNo"] = mPageNo.toString()
        Log.e("PastOpParams:", "$mMap")
        return mMap
    }

    private fun executePastOpByTypeApi() {
        showProgressDialog(activity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.pastOpRequest(mPastOpParams())
            .enqueue(object : Callback<GetAllOpportunitiesByTypeModel> {
                override fun onFailure(call: Call<GetAllOpportunitiesByTypeModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAllOpportunitiesByTypeModel>,
                    response: Response<GetAllOpportunitiesByTypeModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            if(activity!=null){
                            txtNoDataTV.visibility=View.GONE
                            mPastOpList = response.body()?.data as ArrayList<OpportunitiesDataItem?>?
                            setAdapter()
                        }
                        }
                        400 -> {
                            if(activity!=null) {
                                txtNoDataTV.visibility = View.VISIBLE
                            }
                        }
                        401 ->{
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    fun setAdapter() {
        val layoutManager1: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        opportuntiesRV?.layoutManager = layoutManager1
        mPastOpAdapter = UpAdapter(activity, mPastOpList,mLoadMore)
        opportuntiesRV?.adapter = mPastOpAdapter
    }

    var interfaceBottomSheet: InterfaceBottomSheet = object : InterfaceBottomSheet {

        override fun onClickBottomSheet(position: Int, context: Activity?) {
            val dialog = BottomSheetDialog(context!!, R.style.Theme_Design_BottomSheetDialog)
            val view: View = LayoutInflater.from(activity)
                .inflate(R.layout.bottom_sheet_dialog, null)

            var reportLL = view.findViewById<LinearLayout>(R.id.reportLL)
            val btCancelTV = view.findViewById<TextView>(R.id.btCancelTV)

            btCancelTV?.setOnClickListener {
                dialog.dismiss()
            }

            reportLL.setOnClickListener(View.OnClickListener {

                dialog.dismiss()

                // open bottomsheet report
                onClickBottomSheetReport()

            })

            dialog.setCancelable(true)
            dialog.setContentView(view)
            dialog.show()
        }
    }


    fun onClickBottomSheetReport() {
        val dialog = BottomSheetDialog(requireActivity()!!, R.style.Theme_Design_BottomSheetDialog)
        val view: View = LayoutInflater.from(activity)
            .inflate(R.layout.bottom_sheet_dialog_report, null)
        dialog.setCancelable(true)
        dialog.setContentView(view)
        dialog.show()
    }

    override fun mLoadMorePastOp(mModel: OpportunitiesDataItem) {
        if (isLoading) {
            ++mPageNo
            loadMorePastOpByType()
        }
    }

    private fun loadMorePastOpByType() {
        mProgressRL.visibility=View.VISIBLE
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.pastOpRequest(mPastOpParams())
            .enqueue(object : Callback<GetAllOpportunitiesByTypeModel> {
                override fun onFailure(call: Call<GetAllOpportunitiesByTypeModel>, t: Throwable) {
                    mProgressRL.visibility=View.GONE
                }

                override fun onResponse(
                    call: Call<GetAllOpportunitiesByTypeModel>,
                    response: Response<GetAllOpportunitiesByTypeModel>
                ) {
                    mProgressRL.visibility=View.GONE
                    when (response.code()) {
                        200 -> {
                            mPastOpList?.addAll(response.body()?.data!!)
                            mPastOpAdapter?.notifyDataSetChanged()
                            isLoading = !response.body()?.lastPage?.equals(true)!!
                        }
                        401 ->{
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }
}