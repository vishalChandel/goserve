package com.goserve.Activity.Fragment

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import com.goserve.Activity.FilterActivity
import com.goserve.R
import kotlinx.android.synthetic.main.fragment_opportunities.*
import kotlinx.android.synthetic.main.item_search.*
import kotlinx.android.synthetic.main.item_search.view.*
import kotlinx.android.synthetic.main.item_tabs.view.*
import kotlinx.android.synthetic.main.item_tabs.view.servingTV
import com.goserve.Activity.SearchActivity
import kotlinx.android.synthetic.main.fragment_all.*

class OpportunitiesFragment : BasicFragment() {
    // - - Getting the Current Class Name
    override var TAG = this@OpportunitiesFragment.javaClass.simpleName
    // - - Initialize Objects
    var imgSearchRL: RelativeLayout? = null
    var searchRL: RelativeLayout? = null
    var allFillTV: TextView? = null
    var servingTV: TextView? = null
    var tabLL: LinearLayout? = null
    var imgFilterRL: RelativeLayout? = null
    var cancelTV: TextView? = null
    var mainBarRL: RelativeLayout? = null
    var opportuntiesLL: LinearLayout? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_opportunities, container, false)

        allFillTV = view.findViewById(R.id.allFillTV)
        servingTV = view.findViewById(R.id.servingTV)
        tabLL = view.findViewById(R.id.tabLL)
        imgSearchRL = view.findViewById(R.id.imgSearchRL)
        searchRL = view.findViewById(R.id.searchRL)
        imgFilterRL = view.findViewById(R.id.imgFilterRL)
        cancelTV = view.findViewById(R.id.cancelTV)
        mainBarRL = view.findViewById(R.id.mainBarRL);
        opportuntiesLL = view.findViewById(R.id.opportuntiesLL);

        tabSelection(0)

        performCliks()

        return view
    }

    @SuppressLint("NewApi")
    fun performCliks() {

        allFillTV?.setOnClickListener(View.OnClickListener {
            tabSelection(0)
        })

        servingTV?.setOnClickListener(View.OnClickListener {
            tabSelection(1)
        })

        imgFilterRL?.setOnClickListener(View.OnClickListener {
            val intent = Intent(activity, FilterActivity::class.java)
            startActivity(intent)

        })

        imgSearchRL?.setOnClickListener(View.OnClickListener {
            toolbarRL.visibility = View.VISIBLE
            includeTabs.allFillTV.visibility = View.VISIBLE
            includeTabs.servingTV.visibility = View.VISIBLE
            includeTabs.tabLL.visibility = View.VISIBLE
            val intent = Intent(context, SearchActivity::class.java)
            startActivity(intent)
        })

        cancelTV?.setOnClickListener(View.OnClickListener {

            val layoutParams: RelativeLayout.LayoutParams =
                RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.WRAP_CONTENT
                )
            mainBarRL?.layoutParams = layoutParams

          //  sendBroadcast(CANCEL_SEARCH)

            toolbarRL.visibility = View.VISIBLE
            includeTabs.allFillTV.visibility = View.VISIBLE
            includeTabs.servingTV.visibility = View.VISIBLE
            includeTabs.tabLL.visibility = View.VISIBLE

        })
    }

/*    private fun sendBroadcast(type: String) {
        val intent = Intent("Broadcast")
        intent.putExtra(BROADCAST_STATUS, type)
        LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent);
    }*/

    fun tabSelection(pos: Int) {
        allFillTV?.setBackgroundResource(R.drawable.bg_tab_unselect)
        servingTV?.setBackgroundResource(R.drawable.bg_tab_unselect)

        tabLL?.setBackgroundResource(R.drawable.bg_tab_grey)
        if (pos == 0) {
            allFillTV?.setBackgroundResource(R.drawable.bg_tabs_bt)
            switchFragment(AllFragment(), "", false, null)
        } else if (pos == 1) {
            servingTV?.setBackgroundResource(R.drawable.bg_tabs_bt)
            switchFragment(ServingFragment(), "", false, null)
        }
    }


    //to switch between fragments*
    override fun switchFragment(
        fragment: Fragment?,
        Tag: String?,
        addToStack: Boolean,
        bundle: Bundle?
    ) {
        val fragmentManager: FragmentManager = childFragmentManager
        if (fragment != null) {
            val fragmentTransaction = fragmentManager.beginTransaction()
            fragmentTransaction.replace(R.id.frameLayoutOppor, fragment, Tag)
            if (addToStack) fragmentTransaction.addToBackStack(Tag)
            if (bundle != null) fragment.arguments = bundle
            fragmentTransaction.commit()
            fragmentManager.executePendingTransactions()

        }

    }

    // extension function to convert pixels value to dp value
    // this method return integer dp value
    fun Int.pixelsToDpInt(context: Context): Int {
        return this / (context.resources
            .displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT)
    }
}


