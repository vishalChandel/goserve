package com.goserve.Activity.Fragment

import android.app.Activity
import android.app.Dialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.goserve.Adapter.ALLAdapter
import com.goserve.Adapter.ReportReasonsAdapter
import com.goserve.Interfaces.InterfaceBottomSheet
import com.goserve.Interfaces.LoadMoreOpportunities
import com.goserve.Interfaces.OpportunityInterface
import com.goserve.Interfaces.ReportReasonInterface
import com.goserve.R
import com.goserve.model.*
import com.goserve.utils.AppPreference
import com.goserve.utils.SEARCH_TEXT
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.fragment_serving.*
import kotlinx.android.synthetic.main.fragment_serving.txtNoDataTV
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ServingFragment : BasicFragment(), LoadMoreOpportunities {
    // - - Getting the Current Class Name
    override var TAG = this@ServingFragment.javaClass.simpleName

    // - - Initialize Objects
    var mServingOpportunitiesList: ArrayList<OpportunitiesDataItem?>? = ArrayList<OpportunitiesDataItem?>()
    var mReportReasonsList: ArrayList<ReportReasonDataItem?>? = ArrayList<ReportReasonDataItem?>()
    var mALLAdapter: ALLAdapter? = null
    var mReportReasonsAdapter: ReportReasonsAdapter? = null
    var servingTabRV: RecyclerView? = null
    var mPageNo: Int = 1
    var mLoadMore: LoadMoreOpportunities? = null
    var isLoading: Boolean = true
    var mReportedId = ""
    var mReasonID: String = ""
    var opportuntiesServiceLL: LinearLayout? = null
    var oneView: View? = null
    var mDialog: Dialog? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_serving, container, false)
        servingTabRV = view.findViewById(R.id.servingTabRV)
        opportuntiesServiceLL = view.findViewById(R.id.opportuntiesServiceLL)
        oneView = view.findViewById(R.id.oneView)
        executeReportReasonsApi()
        return view
    }

    private fun mReportReasonParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = authToken()
        return mMap
    }

    private fun executeReportReasonsApi() {
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.reportReasonsRequest(mReportReasonParams())
            .enqueue(object : Callback<ReportReasonModel> {
                override fun onFailure(call: Call<ReportReasonModel>, t: Throwable) {
                }

                override fun onResponse(
                    call: Call<ReportReasonModel>,
                    response: Response<ReportReasonModel>
                ) {
                    when (response.code()) {
                        200 -> {
                            mReportReasonsList =
                                response.body()?.data as ArrayList<ReportReasonDataItem?>?
                        }
                        401 -> {
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    private fun mServingOpportunitiesParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = authToken()
        mMap["type"] = "2"
        mMap["search"] = searchText()
        mMap["perPage"] = "10"
        mMap["pageNo"] = mPageNo.toString()
        Log.e("AllOpportunitiesParams:", "$mMap")
        return mMap
    }

    private fun executeServingOpportunitiesApi() {
        showProgressDialog(activity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.getAllOpportunitiesByTypeRequest(mServingOpportunitiesParams())
            .enqueue(object : Callback<GetAllOpportunitiesByTypeModel> {
                override fun onFailure(call: Call<GetAllOpportunitiesByTypeModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAllOpportunitiesByTypeModel>,
                    response: Response<GetAllOpportunitiesByTypeModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            if (activity != null) {
                                txtNoDataTV.visibility = View.GONE
                                servingTabRV!!.visibility = View.VISIBLE
                                mServingOpportunitiesList = response.body()?.data as ArrayList<OpportunitiesDataItem?>?
                                Log.e("ServingList",mServingOpportunitiesList.toString())
                                setAdapter()
                                AppPreference().writeString(activity!!, SEARCH_TEXT, "")
                            }
                        }
                        403 -> {
                            showAccountDisabledDialog(activity)
                        }
                        400 -> {
                            if (activity != null) {
                                txtNoDataTV.visibility = View.VISIBLE
                                servingTabRV!!.visibility = View.GONE
                            }
                        }
                        401 -> {
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    override fun mLoadMoreOpportunities(mModel: OpportunitiesDataItem) {
        if (isLoading) {
            ++mPageNo
            loadMoreOppotunitiesByType()
        }
    }

    private fun loadMoreOppotunitiesByType() {
        mProgressRL.visibility = View.VISIBLE
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.getAllOpportunitiesByTypeRequest(mServingOpportunitiesParams())
            .enqueue(object : Callback<GetAllOpportunitiesByTypeModel> {
                override fun onFailure(call: Call<GetAllOpportunitiesByTypeModel>, t: Throwable) {
                    mProgressRL.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<GetAllOpportunitiesByTypeModel>,
                    response: Response<GetAllOpportunitiesByTypeModel>
                ) {
                    mProgressRL.visibility = View.GONE
                    when (response.code()) {
                        200 -> {
                            mServingOpportunitiesList?.addAll(response.body()?.data!!)
                            mALLAdapter?.notifyDataSetChanged()
                            isLoading = !response.body()?.lastPage?.equals(true)!!
                        }
                        401 -> {
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }


    private fun setAdapter() {
        val layoutManager1: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        servingTabRV?.layoutManager = layoutManager1
        mALLAdapter =
            ALLAdapter(
                activity,
                mServingOpportunitiesList,
                interfaceBottomSheet,
                "Serving",
                mLoadMore,
                mOpportunityInterface
            )
        servingTabRV?.adapter = mALLAdapter
    }

    var interfaceBottomSheet: InterfaceBottomSheet = object : InterfaceBottomSheet {

        override fun onClickBottomSheet(position: Int, context: Activity?) {
            val dialog = BottomSheetDialog(context!!, R.style.Theme_Design_BottomSheetDialog)
            val view: View = LayoutInflater.from(activity)
                .inflate(R.layout.bottom_sheet_dialog, null)

            var reportLL = view.findViewById<LinearLayout>(R.id.reportLL)
            val btCancelTV = view.findViewById<TextView>(R.id.btCancelTV)

            btCancelTV?.setOnClickListener {
                dialog.dismiss()
            }

            reportLL.setOnClickListener(View.OnClickListener {

                dialog.dismiss()

                // open bottomsheet report
                onClickBottomSheetReport()

            })

            dialog.setCancelable(true)
            dialog.setContentView(view)
            dialog.show()
        }
    }


    fun onClickBottomSheetReport() {
        mDialog = BottomSheetDialog(requireActivity()!!, R.style.Theme_Design_BottomSheetDialog)
        val view: View =
            LayoutInflater.from(activity).inflate(R.layout.bottom_sheet_dialog_report, null)
        mDialog!!.setCancelable(true)
        mDialog!!.setContentView(view)
        val reportReasonsRV = mDialog!!.findViewById<RecyclerView>(R.id.reportReasonsRV)
        val layoutManager: RecyclerView.LayoutManager =
            LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        reportReasonsRV!!.layoutManager = layoutManager
        mReportReasonsAdapter =
            ReportReasonsAdapter(activity, mReportReasonsList, mReportReasonInterface)
        reportReasonsRV!!.adapter = mReportReasonsAdapter
        mDialog!!.show()
    }

    private fun mFilterOpParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = authToken()
        mMap["type"] = "2"
        mMap["startDate"] = starDate()
        mMap["endDate"] = endDate()
        mMap["selectDay"] = selectDay()
        mMap["pageNo"] = "1"
        mMap["perPage"] = "10"
        Log.e("FilterOpParams:", "$mMap")
        return mMap
    }

    private fun executeFilterOpApi() {
        showProgressDialog(activity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.filterOpportunitiesRequest(mFilterOpParams())
            .enqueue(object : Callback<GetAllOpportunitiesByTypeModel> {
                override fun onFailure(call: Call<GetAllOpportunitiesByTypeModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAllOpportunitiesByTypeModel>,
                    response: Response<GetAllOpportunitiesByTypeModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            servingTabRV!!.visibility=View.VISIBLE
                            txtNoDataTV.visibility = View.GONE
                            mServingOpportunitiesList = response.body()?.data as java.util.ArrayList<OpportunitiesDataItem?>?
                            Log.e("FilterList",mServingOpportunitiesList.toString())
                            setAdapter()
                        }
                        403 -> {
                            showAccountDisabledDialog(activity)
                        }
                        400 -> {
                            servingTabRV!!.visibility=View.GONE
                            txtNoDataTV.visibility = View.VISIBLE
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    private var mOpportunityInterface = object : OpportunityInterface {
        override fun mInterface(mModel: OpportunitiesDataItem?, position: Int) {
            mReportedId = mModel?.opID.toString()
        }
    }

    private var mReportReasonInterface = object : ReportReasonInterface {
        override fun mInterface(mModel: ReportReasonDataItem?, position: Int) {
            mReasonID = mModel?.reasonId.toString()
            executeAddReportApi()
        }
    }

    private fun mAddReportParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = authToken()
        mMap["reasonId"] = mReasonID
        mMap["reportedId"] = mReportedId
        mMap["reportType"] = "1"
        return mMap
    }

    private fun executeAddReportApi() {
        showProgressDialog(activity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.addReportRequest(mAddReportParams())
            .enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            showToast(activity, getString(R.string.reported_successfully))
                            mDialog!!.dismiss()
                        }
                        401 -> {
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    override fun onResume() {
        super.onResume()
        if (filter() == "filter") {
            mServingOpportunitiesList!!.clear()
            executeFilterOpApi()
        } else {
            mLoadMore = this
            if (mServingOpportunitiesList != null) {
                mServingOpportunitiesList!!.clear()
            }
            executeServingOpportunitiesApi()
        }
    }
}