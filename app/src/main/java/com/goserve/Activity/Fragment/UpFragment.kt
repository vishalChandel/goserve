package com.goserve.Activity.Fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.goserve.Adapter.UpAdapter
import com.goserve.Interfaces.LoadMorePastOp
import com.goserve.R
import com.goserve.model.GetAllOpportunitiesByTypeModel
import com.goserve.model.OpportunitiesDataItem
import com.goserve.model.PastOpByTypeModel
import com.goserve.model.PastOpDataItem
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.fragment_up.*
import kotlinx.android.synthetic.main.fragment_up.txtNoDataTV
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class UpFragment : BasicFragment(),LoadMorePastOp {
    // - - Getting the Current Class Name
    override var TAG = this@UpFragment.javaClass.simpleName
    // - - Initialize Objects
    var mPastOpList: ArrayList<OpportunitiesDataItem?>? = ArrayList<OpportunitiesDataItem?>()
    var mUpAdapter: UpAdapter? = null
    var upRV: RecyclerView? = null
    var mPageNo: Int = 1
    var mLoadMore: LoadMorePastOp? = null
    var isLoading: Boolean = true

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View =inflater.inflate(R.layout.fragment_up, container, false)
        upRV = view.findViewById(R.id.upRV)
        executePastOpByTypeApi()
        return view
    }

    private fun mPastOpParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = authToken()
        mMap["type"] = "1"
        mMap["perPage"] = "20"
        mMap["pageNo"] = "1"
        Log.e("ComingPastOpParams:", "$mMap")
        return mMap
    }

    private fun executePastOpByTypeApi() {
        showProgressDialog(activity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.pastOpRequest(mPastOpParams())
            .enqueue(object : Callback<GetAllOpportunitiesByTypeModel> {
                override fun onFailure(call: Call<GetAllOpportunitiesByTypeModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAllOpportunitiesByTypeModel>,
                    response: Response<GetAllOpportunitiesByTypeModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            if(activity!=null) {
                                txtNoDataTV.visibility = View.GONE
                                mPastOpList = response.body()?.data as ArrayList<OpportunitiesDataItem?>?
                                setAdapter()
                            }
                        }
                        400 -> {
                            if(activity!=null) {
                                txtNoDataTV.visibility = View.VISIBLE
                            }
                        }
                        401 ->{
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }


    private fun setAdapter() {
        val layoutManager1: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        upRV?.layoutManager = layoutManager1
        mUpAdapter = UpAdapter(activity, mPastOpList, mLoadMore)
        upRV?.adapter = mUpAdapter
    }

    override fun mLoadMorePastOp(mModel: OpportunitiesDataItem) {
        if (isLoading) {
            ++mPageNo
            loadMorePastOpByType()
        }
    }

    private fun loadMorePastOpByType() {
        mProgressRL.visibility=View.VISIBLE
        val mApiInterface: ApiInterface = RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.pastOpRequest(mPastOpParams())
            .enqueue(object : Callback<GetAllOpportunitiesByTypeModel> {
                override fun onFailure(call: Call<GetAllOpportunitiesByTypeModel>, t: Throwable) {
                    mProgressRL.visibility=View.GONE
                }

                override fun onResponse(
                    call: Call<GetAllOpportunitiesByTypeModel>,
                    response: Response<GetAllOpportunitiesByTypeModel>
                ) {
                    mProgressRL.visibility=View.GONE
                    when (response.code()) {
                        200 -> {
                            mPastOpList?.addAll(response.body()?.data!!)
                            mUpAdapter?.notifyDataSetChanged()
                            isLoading = !response.body()?.lastPage?.equals(true)!!
                        }
                        401 ->{
                            showAuthFailedDialog(activity)
                        }
                        500 -> {
                            showToast(activity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }
}