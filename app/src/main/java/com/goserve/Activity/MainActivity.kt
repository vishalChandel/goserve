package com.goserve.Activity

import android.app.Activity
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.widget.FrameLayout
import androidx.annotation.RequiresApi
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationBarView
import com.goserve.Activity.Fragment.*
import com.goserve.R
import com.goserve.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.activity_filter.*
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.item_search.*
import kotlinx.android.synthetic.main.item_search.view.*
import kotlinx.android.synthetic.main.item_tabs.view.*

class MainActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@MainActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@MainActivity
    private lateinit var binding: ActivityMainBinding
    lateinit var navigationView: BottomNavigationView
    lateinit var frameLayout: FrameLayout
    private var isOpportunitiesClicked = false
    private var isNotificationClicked = false
    private var isAccountClicked = false


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setStatusBar(mActivity, Color.WHITE)

        navigationView = findViewById(R.id.navigationId)
        frameLayout = findViewById(R.id.frameLayout)
        if (intent != null) {
            if (intent.getStringExtra("Push") != null && intent.getStringExtra("Push").equals("push")) {
                switchFragment(NotificationFragment(), "", false, null)
                navigationView.selectedItemId=R.id.actionNotification
                isOpportunitiesClicked=false
                isAccountClicked=false
                isNotificationClicked=true
            } else {
                isNotificationClicked=false
                isAccountClicked=false
                isOpportunitiesClicked=true
                navigationView.selectedItemId=R.id.actionOpportunties
                switchFragment(OpportunitiesFragment(), "", false, null)
            }
        }
        navigationView.setOnItemSelectedListener(NavigationBarView.OnItemSelectedListener {
            when (it.itemId) {
                R.id.actionOpportunties -> {
                   if(isOpportunitiesClicked==false) {
                       switchFragment(OpportunitiesFragment(), "", false, null)
                   }
                    isNotificationClicked=false
                    isAccountClicked=false
                    isOpportunitiesClicked=true
                }
                R.id.actionNotification -> {
                    if(isNotificationClicked==false){
                    switchFragment(NotificationFragment(), "", false, null)
                }
                    isOpportunitiesClicked=false
                    isAccountClicked=false
                    isNotificationClicked=true
                }
                R.id.actionFavorite -> {
                    if (isAccountClicked == false) {
                        switchFragment(AccountFragment(), "", false, null)
                    }
                    isNotificationClicked=false
                    isOpportunitiesClicked=false
                    isAccountClicked=true
                }
            }
            true

        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}