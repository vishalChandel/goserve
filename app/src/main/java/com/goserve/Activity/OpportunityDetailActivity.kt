package com.goserve.Activity

import android.Manifest
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.goserve.R
import com.goserve.databinding.ActivityOpportunityDetailBinding
import com.goserve.model.NotificationDataItem
import com.goserve.model.OpportunitiesDataItem
import com.goserve.model.StatusMsgModel
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.provider.CalendarContract.Events
import android.content.ContentValues
import android.net.Uri
import android.provider.CalendarContract

import android.content.ContentResolver

import android.content.pm.PackageManager

import androidx.core.app.ActivityCompat

class OpportunityDetailActivity : BasicActivity() {

    // - - Initialize Objects
    var binding: ActivityOpportunityDetailBinding? = null
    private var mTag = ""
    var mPos: Int? = null
    var mOpId: String? = null
    var opportunitiesLiting = ArrayList<OpportunitiesDataItem?>()
    var mNotificationOpportunitiesLiting = ArrayList<NotificationDataItem?>()
    var mStartDateTimeStamp = ""
    var mEndDateTimeStamp = ""
    var mStartTimeStampJson = ""
    var mEndTimeStampJson = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOpportunityDetailBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)
        getIntentData()
        onViewClicked()
    }

    private fun setDataOnWidgets() {
        binding!!.txtOpportunityTitleTV.text = opportunitiesLiting[mPos!!]?.title
        binding!!.txtDescriptionTV.text = opportunitiesLiting[mPos!!]?.description
        binding!!.txtNoOfHoursTV.text = opportunitiesLiting[mPos!!]?.opHour
        binding!!.txtNumberOfPeopleTV.text = opportunitiesLiting[mPos!!]?.peopleRequired
        binding!!.txtEmailTV.text = opportunitiesLiting[mPos!!]?.email
        binding!!.txtTimeTV.text = opportunitiesLiting[mPos!!]?.startTime.toString() + " (EST) - " + opportunitiesLiting[mPos!!]?.endTime.toString()+ " (EST)"
        binding!!.txtAddressTV.text = opportunitiesLiting[mPos!!]?.opAddress
        binding!!.txtDateTV.text =
            opportunitiesLiting[mPos!!]?.startDateInDate + " - " + opportunitiesLiting[mPos!!]?.endDateInDate
        binding!!.txtNumberofDaysTV.text = opportunitiesLiting[mPos!!]?.dayDiff
        Glide
            .with(this)
            .load(opportunitiesLiting[mPos!!]?.opImage)
            .error(R.drawable.img_app_placeholder2)
            .placeholder(R.drawable.img_app_placeholder2)
            .into(binding!!.imgOppportunityIV)
    }

    private fun getIntentData() {
        if (intent != null) {
            if (intent.getStringExtra("Push") != null && intent.getStringExtra("Push")
                    .equals("push")
            ) {
                var mJsonObject = JSONObject(intent.getStringExtra("mOpDetailsJsonObject"))
                binding!!.txtOpportunityTitleTV.text = mJsonObject.getString("title")
                binding!!.txtDescriptionTV.text = mJsonObject.getString("description")
                binding!!.txtNoOfHoursTV.text = mJsonObject.getString("opHour")
                binding!!.txtNumberOfPeopleTV.text = mJsonObject.getString("peopleRequired")
                binding!!.txtEmailTV.text = mJsonObject.getString("email")
                binding!!.txtTimeTV.text = mJsonObject.getString("startTime")+ " (EST) - " + mJsonObject.getString("endTime")+ " (EST)"
                binding!!.txtAddressTV.text = mJsonObject.getString("title")
                binding!!.txtDateTV.text = mJsonObject.getString("startDateInDate") + " - " + mJsonObject.getString("endDateInDate")
                binding!!.txtNumberofDaysTV.text = mJsonObject.getString("dayDiff")
                Glide
                    .with(this)
                    .load(mJsonObject.getString("opImage"))
                    .error(R.drawable.img_app_placeholder2)
                    .placeholder(R.drawable.img_app_placeholder2)
                    .into(binding!!.imgOppportunityIV)
                binding!!.txtJoinOpportunityTV.visibility = View.GONE
            }
            else if (intent.getStringExtra("Tag") != null && intent.getStringExtra("Tag")
                    .equals("Notification")
            ) {
                binding!!.txtJoinOpportunityTV.visibility = View.GONE
                mTag = intent.getStringExtra("Tag").toString()
                mPos = intent.getStringExtra("pos")?.toInt()
                mNotificationOpportunitiesLiting =
                    intent.getSerializableExtra("opportunitiesNData") as ArrayList<NotificationDataItem?>
                mOpId = mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.opID
                binding!!.txtOpportunityTitleTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.title
                binding!!.txtDescriptionTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.description
                binding!!.txtNoOfHoursTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.opHour
                binding!!.txtNumberOfPeopleTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.peopleRequired
                binding!!.txtEmailTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.email
                binding!!.txtTimeTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.startTimeForAddEvent.toString() + " (EST) - " +
                        mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.endTime.toString()+ " (EST)"


                binding!!.txtAddressTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.opAddress
                binding!!.txtDateTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.startDateInDate + " - " + mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.endDateInDate
                binding!!.txtNumberofDaysTV.text =
                    mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.dayDiff
                Glide
                    .with(this)
                    .load(mNotificationOpportunitiesLiting[mPos!!]?.opportunityDetails?.opImage)
                    .error(R.drawable.img_app_placeholder2)
                    .placeholder(R.drawable.img_app_placeholder2)
                    .into(binding!!.imgOppportunityIV)
                binding!!.txtJoinOpportunityTV.visibility = View.GONE
            } else {
                mTag = intent.getStringExtra("Tag").toString()
                mPos = intent.getStringExtra("pos")?.toInt()
                opportunitiesLiting =
                    intent.getSerializableExtra("opportunitiesData") as ArrayList<OpportunitiesDataItem?>
                mOpId = opportunitiesLiting[mPos!!]?.opID
                if (intent.getStringExtra("Tag") != null && intent.getStringExtra("Tag")
                        .equals("Serving")
                ) {
                    mStartDateTimeStamp = opportunitiesLiting[mPos!!]?.startTimeForAddEvent!!
                    mEndDateTimeStamp = opportunitiesLiting[mPos!!]?.endTimeForAddEvent!!
                    binding!!.txtJoinOpportunityTV.text = "Accepted"
                    binding!!.imgAddEventIV.visibility = View.VISIBLE
                } else if (intent.getStringExtra("Tag") != null && intent.getStringExtra("Tag")
                        .equals("AccountTab")
                ) {
                    binding!!.txtJoinOpportunityTV.visibility = View.GONE
                } else if (intent.getStringExtra("Tag") != null && intent.getStringExtra("Tag")
                        .equals("All")
                ) {
                    if (opportunitiesLiting[mPos!!]?.isStatus == "0" || opportunitiesLiting[mPos!!]?.isStatus == "3") {
                        binding!!.txtJoinOpportunityTV.text = "Join Opportunity"
                    } else if (opportunitiesLiting[mPos!!]?.isStatus == "1") {
                        binding!!.txtJoinOpportunityTV.text = "Pending Approval"
                    }
                }
                setDataOnWidgets()

            }
        }
    }


    private fun onViewClicked() {
        binding!!.backRL.setOnClickListener {
            onBackPressed()
        }
        binding!!.imgAddEventIV.setOnClickListener {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CALENDAR,Manifest.permission.WRITE_CALENDAR),1)
            } else if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) == PackageManager.PERMISSION_GRANTED) {
                var mStartYear = 0
                var mStartMonth = 0
                var mStartDate = 0
                var mStartHour = 0
                var mStartMin=0
                var mEndYear = 0
                var mEndMonth = 0
                var mEndDate = 0
                var mEndHour = 0
                var mEndMin=0
                var startMillis: Long = 0
                var endMillis: Long = 0

                val d = Date(mStartDateTimeStamp.toLong() * 1000)
                Log.e("mStartDateTimeStamp", mStartDateTimeStamp)
                val D = Date(mEndDateTimeStamp.toLong() * 1000)
                val yearFormatter = SimpleDateFormat("yyyy")
                val monthFormatter = SimpleDateFormat("MM")
                val dateFormatter = SimpleDateFormat("dd")
                val hourFormatter = SimpleDateFormat("hh")
                val minFormatter = SimpleDateFormat("mm")
                mStartYear = yearFormatter.format(d).toInt()
                mEndYear = yearFormatter.format(D).toInt()
                mStartMonth = monthFormatter.format(d).toInt()
                mEndMonth = monthFormatter.format(D).toInt()
                mStartDate = dateFormatter.format(d).toInt()
                mEndDate = dateFormatter.format(D).toInt()
                mStartHour = hourFormatter.format(d).toInt()
                mEndHour = hourFormatter.format(D).toInt()
                mStartMin = minFormatter.format(d).toInt()
                mEndMin = minFormatter.format(D).toInt()
                val beginTime = Calendar.getInstance()
                beginTime.set(mStartYear, mStartMonth - 1, mStartDate, mStartHour, mStartMin)
                val endTime = Calendar.getInstance()
                endTime.set(mEndYear, mEndMonth - 1, mEndDate, mEndHour, mEndMin )
                startMillis = beginTime.timeInMillis
                endMillis = endTime.timeInMillis
                val cr = contentResolver
                val values = ContentValues()
                values.put(Events.DTSTART, startMillis)
                values.put(Events.DTEND, endMillis)
                values.put(Events.TITLE, opportunitiesLiting[mPos!!]?.title.toString())
                values.put(Events.DESCRIPTION, "")
                values.put(Events.CALENDAR_ID, 1)
                values.put(Events.EVENT_TIMEZONE, "Indian/Christmas")
                val uri: Uri? = cr.insert(Events.CONTENT_URI, values)
                showToast(mActivity, "Even Added Successfully.")
            }

        }

        binding!!.txtJoinOpportunityTV.setOnClickListener {
            if (intent.getStringExtra("Tag") != null && intent.getStringExtra("Tag")
                    .equals("All")
            ) {
                if (opportunitiesLiting[mPos!!]?.isStatus == "0" || opportunitiesLiting[mPos!!]?.isStatus == "3") {
                    executeJoinOpApi()
                } else {

                }
            }
        }

    }

    private fun mJoinOpParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = getAuthToken()
        mMap["opID"] = mOpId
        Log.e("JoinOpParams:", "$mMap")
        return mMap
    }

    private fun executeJoinOpApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.joinOpportunitiesRequest(mJoinOpParams())
            .enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            binding!!.txtJoinOpportunityTV.text = "Pending Approval"
                        }
                        401 -> {
                            showAuthFailedDialog(mActivity)
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    // - - Get Date/Time from TimeStamp
    private fun convertLongToStringDate(mLong: String): String {
        val d = Date(mLong.toLong() * 1000).toString()
        val formatter3 = SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
        var value = formatter3.parse(d)
        val dateFormatter = SimpleDateFormat("hh:mm a")
        dateFormatter.timeZone = TimeZone.getDefault()
        var ourDate: String = dateFormatter.format(value)
        return ourDate

    }

    override fun onBackPressed() {
        if (intent != null) {
            if (intent.getStringExtra("Push") != null && intent.getStringExtra("Push")
                    .equals("push")
            ) {
                val i = Intent(mActivity, MainActivity::class.java)
                startActivity(i)
                finish()
            } else {
                super.onBackPressed()
            }
        }
    }

}