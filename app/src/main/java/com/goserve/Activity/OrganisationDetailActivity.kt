package com.goserve.Activity

import android.app.AlertDialog
import android.content.*
import android.graphics.Color
import android.os.Bundle
import com.bumptech.glide.Glide
import com.goserve.R
import com.goserve.databinding.ActivityOrganisationDetailBinding
import android.os.Build

import android.database.Cursor
import android.net.Uri
import android.provider.CalendarContract
import java.util.*


class OrganisationDetailActivity : BasicActivity() {

    // - - Initialize Objects
    var binding: ActivityOrganisationDetailBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrganisationDetailBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)
        getIntentData()
        onViewClicked()
    }

    private fun getIntentData() {
        if (intent != null) {
            if (intent.getStringExtra("Push") != null && intent.getStringExtra("Push").equals("push")) {
               binding!!.txtTitleTV.text=intent.getStringExtra("orgTitle")
                binding!!.txtDetailsTV.text=intent.getStringExtra("orgDetail")
                Glide
                    .with(this)
                    .load(intent.getStringExtra("orgImage"))
                    .error(R.drawable.img_app_placeholder2)
                    .placeholder(R.drawable.img_app_placeholder2)
                    .into(binding!!.imgImageIV)
            }
            else {
                setDataOnWidgets()
            }
        }
    }

    private fun onViewClicked() {
        binding!!.backRL.setOnClickListener {
            onBackPressed()
        }
    }

    fun setDataOnWidgets() {
        binding!!.txtTitleTV.text = orgTitle()
        binding!!.txtDetailsTV.text = orgDetail()
        Glide
            .with(this)
            .load(orgImage())
            .error(R.drawable.img_app_placeholder2)
            .placeholder(R.drawable.img_app_placeholder2)
            .into(binding!!.imgImageIV)
    }



    override fun onBackPressed() {
        if (intent != null) {
            if (intent.getStringExtra("Push") != null && intent.getStringExtra("Push").equals("push")) {
                val i = Intent(mActivity, MainActivity::class.java)
                startActivity(i)
                finish()
            }
            else {
                super.onBackPressed()
            }
        }
    }
}