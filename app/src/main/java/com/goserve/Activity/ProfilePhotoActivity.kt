package com.goserve.Activity

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Build.MODEL
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.goserve.R
import com.goserve.databinding.ActivityProfilePhotoBinding
import kotlinx.android.synthetic.main.activity_edit_profile.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.jar.Manifest

class ProfilePhotoActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@ProfilePhotoActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@ProfilePhotoActivity
    var binding: ActivityProfilePhotoBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfilePhotoBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        setStatusBar(mActivity, Color.WHITE)

        performClick()

    }

    fun performClick() {

        binding?.imgArrowRL?.setOnClickListener(View.OnClickListener {
            onBackPressed()

        })

        binding?.btCancelTV?.setOnClickListener(View.OnClickListener {
            onBackPressed()

        })

    }

}