package com.goserve.Activity

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.View
import com.goserve.R
import com.goserve.databinding.ActivityReportBinding
import com.goserve.model.StatusMsgModel
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ReportActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@ReportActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@ReportActivity

    var binding: ActivityReportBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReportBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)
        setDataOnWidgets()
        // select background
        backgroundSelector()
        // clicks
        performClick()
    }

    private fun setDataOnWidgets() {
            binding!!.NameET.text=firstName() +" "+ lastName()
            binding!!.emailET.setText(userEmail())
    }

    fun backgroundSelector() {
        editTextSelector(binding?.messageET!!, binding?.messageRL!!, "")

    }

    fun performClick() {

        binding!!.imgArrowRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        binding!!.btSendQueryTV.setOnClickListener(View.OnClickListener {
            if (isValidate()) {
                executeContactUsApi()
            }
        })
    }

    private fun mContactUsParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = getAuthToken()
        mMap["name"] = firstName() +" "+ lastName()
        mMap["email"] = userEmail()
        mMap["message"] = binding!!.messageET.text.toString()
        return mMap
    }

    private fun executeContactUsApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.contactusRequest(mContactUsParams())
            .enqueue(object : Callback<StatusMsgModel> {
                override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<StatusMsgModel>,
                    response: Response<StatusMsgModel>
                ) {dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            showToast(mActivity,getString(R.string.messaged_successfully))
                            binding!!.messageET.text.clear()
                        }
                        401 ->{
                            showAuthFailedDialog(mActivity)
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }
            })
    }

    // Set up validations
    private fun isValidate(): Boolean {
        var flag = true
        if (binding?.messageET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_your_message))
            flag = false
        }
        return flag
    }
}