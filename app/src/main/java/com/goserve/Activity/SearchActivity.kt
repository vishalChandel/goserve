package com.goserve.Activity

import android.app.Activity
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.TextView
import com.goserve.databinding.ActivitySearchBinding
import com.goserve.utils.AppPreference
import com.goserve.utils.SEARCH_TEXT

class SearchActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@SearchActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@SearchActivity

    var binding: ActivitySearchBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySearchBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)
        AppPreference().writeString(mActivity!!, SEARCH_TEXT, binding!!.editSearchET.text.toString())
        performClick()
        searchText()
    }


    fun performClick() {
        binding!!.cancelTV.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
    }

     fun searchText() {
         binding!!.editSearchET.setOnEditorActionListener(TextView.OnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                AppPreference().writeString(mActivity!!, SEARCH_TEXT, binding!!.editSearchET.text.toString().toLowerCase())
               onBackPressed()
            }
            return@OnEditorActionListener true
        })
    }
}