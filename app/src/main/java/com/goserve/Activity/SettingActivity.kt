package com.goserve.Activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.TextView
import com.goserve.R
import com.goserve.databinding.ActivitySettingBinding
import com.goserve.model.StatusMsgModel
import com.goserve.utils.*
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SettingActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@SettingActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@SettingActivity

    var binding: ActivitySettingBinding? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)

        performClick()
    }

    fun performClick() {
        binding!!.imgArrowRL.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        binding!!.changePasswordLL.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, ChangePasswordActivity::class.java)
            startActivity(intent)
        })

        binding!!.editProfileLL.setOnClickListener(View.OnClickListener {
            val intent = Intent(mActivity, EditProfileActivity::class.java)
            startActivity(intent)
        })
        binding!!.aboutLL.setOnClickListener(View.OnClickListener {
            val i = Intent(mActivity, WebViewActivity::class.java)
            i.putExtra(LINK_TYPE, LINK_ABOUT)
            startActivity(i)
        })


        binding!!.termLL.setOnClickListener(View.OnClickListener {
            val i = Intent(mActivity, WebViewActivity::class.java)
            i.putExtra(LINK_TYPE, LINK_TERMS)
            startActivity(i)
        })

        binding!!.privacyLL.setOnClickListener(View.OnClickListener {
            val i = Intent(mActivity, WebViewActivity::class.java)
            i.putExtra(LINK_TYPE, LINK_PP)
            startActivity(i)
        })

        binding!!.logoutLL.setOnClickListener(View.OnClickListener {
            showLogoutConfirmAlertDialog()
        })

        binding!!.reportLL.setOnClickListener(View.OnClickListener {
            val intent = Intent(this, ReportActivity::class.java)
            startActivity(intent)
        })

    }

    // - - Logout Alert Dialog
    private fun showLogoutConfirmAlertDialog() {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }
        btnYes.setOnClickListener {
            alertDialog.dismiss()
            executeLogoutApi()
        }
        alertDialog.show()
    }

    private fun mLogoutParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["authToken"] = getAuthToken()
        Log.e("LogoutParams:", "$mMap")
        return mMap
    }

    private fun executeLogoutApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.logoutRequest(mLogoutParams()).enqueue(object : Callback<StatusMsgModel> {
            override fun onFailure(call: Call<StatusMsgModel>, t: Throwable) {
                dismissProgressDialog()
            }

            override fun onResponse(
                call: Call<StatusMsgModel>,
                response: Response<StatusMsgModel>
            ) {
                dismissProgressDialog()
                when (response.code()) {
                    200 -> {
                        clearSharedPreferenceData()
                    }
                    401 -> {
                        showAuthFailedDialog(mActivity)
                    }
                    500 -> {
                        showToast(mActivity, getString(R.string.internal_server_error))
                    }
                }
            }
        })
    }

    // - - To Clear Shared Preference Data
    private fun clearSharedPreferenceData() {
        val preferences: SharedPreferences = AppPreference().getPreferences(this)
        val editor = preferences.edit()
        editor.clear()
        editor.apply()
        editor.commit()
        val mIntent = Intent(mActivity, LoginActivity::class.java)
        // To clean up all activities
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        mActivity?.startActivity(mIntent)
        mActivity?.finish()
        mActivity?.finishAffinity()
    }


}