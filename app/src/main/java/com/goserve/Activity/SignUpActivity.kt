package com.goserve.Activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.aigestudio.wheelpicker.WheelPicker
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.firebase.FirebaseApp
import com.google.firebase.messaging.FirebaseMessaging
import com.goserve.R
import com.goserve.databinding.ActivitySignUpBinding
import com.goserve.model.GetAllOrganisationModel
import com.goserve.model.OrganisationDataItem
import com.goserve.model.UserDataModel
import com.goserve.utils.*
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Converter
import retrofit2.Response
import java.io.IOException
import java.util.*

class SignUpActivity : BasicActivity() {
    /*** Getting the Current Class Name */
    override var TAG = this@SignUpActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@SignUpActivity
    var binding: ActivitySignUpBinding? = null
    private var mDeviceToken: String = ""
    var mCheck: Boolean = false
    private var mGoogleFirstName: String = ""
    private var mGoogleLastName: String = ""
    private var mGoogleUserId: String = ""
    private var mGoogleEmail: String = ""
    private var mGoogleProfileImage: String = ""
    private var mOrgName: String = ""
    private var mOrgId: String = ""
    var mOrganisationList: ArrayList<OrganisationDataItem?>? = ArrayList<OrganisationDataItem?>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(binding?.root)
        setStatusBar(mActivity, Color.WHITE)
        //select background
        backgroundSelector()
        binding!!.yourBirthTV.showSoftInputOnFocus = false
        executeOrganisationApi()
        setColorText()
        performClick()
        textTouchSelector()
        getIntentData()
        getDeviceToken()
    }

    // - - To Get Device Token
    private fun getDeviceToken() {
        FirebaseApp.initializeApp(this)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.e(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            mDeviceToken = task.result!!
            Log.e("DeviceToken:", "DeviceToken:$mDeviceToken")
        })
    }

    private fun getIntentData() {
        if (intent != null) {
            if (intent.getStringExtra("Type") != null && intent.getStringExtra("Type")
                    .equals("google")
            ) {
                binding!!.passwordTV.visibility = View.GONE
                binding!!.passwordRL.visibility = View.GONE
                binding!!.confirmPasswordTV.visibility = View.GONE
                binding!!.confirmPasswordRL.visibility = View.GONE
                mGoogleUserId = intent.getStringExtra("mGoogleUserId").toString()
                mGoogleEmail = intent.getStringExtra("mGoogleEmail").toString()
                mGoogleFirstName = intent.getStringExtra("mGoogleFirstName").toString()
                mGoogleLastName = intent.getStringExtra("mGoogleLastName").toString()
                mGoogleProfileImage = intent.getStringExtra("mGoogleProfileImage").toString()
                setDataOnWidgets()
            } else if (intent.getStringExtra("Type") != null && intent.getStringExtra("Type")
                    .equals("apple")
            ) {
                binding!!.passwordTV.visibility = View.GONE
                binding!!.passwordRL.visibility = View.GONE
                binding!!.confirmPasswordTV.visibility = View.GONE
                binding!!.confirmPasswordRL.visibility = View.GONE
                mGoogleUserId = intent.getStringExtra("mGoogleUserId").toString()
                Log.i("mGoogleUserId", mGoogleUserId)
                mGoogleEmail = intent.getStringExtra("mGoogleEmail").toString()
                binding!!.emailET.setText(mGoogleEmail)
            }
        }
    }

    private fun setDataOnWidgets() {
        binding!!.emailET.setText(mGoogleEmail)
        binding!!.firstNameET.setText(mGoogleFirstName)
        binding!!.lastNameET.setText(mGoogleLastName)
    }

    private fun textTouchSelector() {
        binding!!.spinnerTV.setOnTouchListener(View.OnTouchListener { v, event ->
            changeBGMethod(binding!!.organizationRL)
            createRelationshipDialog(mActivity)
            false
        })

        binding!!.confirmPasswordET.setOnTouchListener(View.OnTouchListener { v, event ->
            changeBGMethod(binding!!.confirmPasswordET)
            false
        })
    }

    private fun changeBGMethod(mView: View) {
        binding!!.yourBirthRL.setBackgroundResource(R.drawable.bg_outline_gray)
        binding!!.firstNameRL.setBackgroundResource(R.drawable.bg_outline_gray)
        binding!!.lastNameRL.setBackgroundResource(R.drawable.bg_outline_gray)
        binding!!.emailRL.setBackgroundResource(R.drawable.bg_outline_gray)
        binding!!.passwordRL.setBackgroundResource(R.drawable.bg_outline_gray)
        binding!!.confirmPasswordRL.setBackgroundResource(R.drawable.bg_outline_gray)
        binding!!.chooseRL.setBackgroundResource(R.drawable.bg_outline_gray)
        binding!!.organizationRL.setBackgroundResource(R.drawable.bg_outline_gray)

        if (mView == binding!!.spinnerTV) {
            binding!!.organizationRL.setBackgroundResource(R.drawable.bg_login_outline_blue)
        } else if (mView == binding!!.confirmPasswordET) {
            binding!!.confirmPasswordRL.setBackgroundResource(R.drawable.bg_login_outline_blue)
        }
    }


    // your birthday set bg gray
    override fun editTextSelector(mEditText: View, rl: RelativeLayout, tag: String) {
        mEditText.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
            binding!!.yourBirthRL.setBackgroundResource(R.drawable.bg_outline_gray)
            binding!!.organizationRL.setBackgroundResource(R.drawable.bg_outline_gray)
            if (hasFocus) {
                rl.setBackgroundResource(R.drawable.bg_login_outline_blue)
            } else {
                rl.setBackgroundResource(R.drawable.bg_outline_gray)
            }
        }
    }

    fun backgroundSelector() {
        editTextSelector(binding?.firstNameET!!, binding!!.firstNameRL, "")
        editTextSelector(binding!!.lastNameET, binding!!.lastNameRL, "")
        editTextSelector(binding!!.emailET, binding!!.emailRL, "")
        editTextSelector(binding!!.chooseET, binding!!.chooseRL, "")
        editTextSelector(binding!!.passwordET, binding!!.passwordRL, "")
        editTextSelector(binding!!.confirmPasswordET, binding!!.confirmPasswordRL, "")
    }

    fun performClick() {
        binding!!.yourBirthRL.setOnClickListener {
            setDatePicker(binding!!.yourBirthTV)
        }

        binding?.btSignUpTV?.setOnClickListener(View.OnClickListener {
            if (intent.getStringExtra("Type") != null && intent.getStringExtra("Type")
                    .equals("google")
            ) {
                if (isGoogleValidate()) {
                    executeGoogleApi()
                }
            } else if (intent.getStringExtra("Type") != null && intent.getStringExtra("Type")
                    .equals("apple")
            ) {
                if (isGoogleValidate()) {
                    executeAppleApi()
                }

            } else {
                if (isValidate()) {
                    if (isNetworkAvailable(mActivity)) {
                        executeSignupApi()
                    } else
                        showToast(mActivity, getString(R.string.internet_connection_error))
                }
            }
        })

        binding?.txtSignInTV?.setOnClickListener(View.OnClickListener
        {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        })

        binding?.passEyeRL?.setOnClickListener(View.OnClickListener
        {

            setPasswordHideShow(binding!!.passwordET, binding?.passEyeIV!!)

        })

        binding?.conPassassEyeRL?.setOnClickListener(View.OnClickListener
        {

            setPasswordHideShow(binding!!.confirmPasswordET, binding!!.conPassEyeIV)

        })

        binding?.rememRL?.setOnClickListener(View.OnClickListener
        {
            if (mCheck) {
                binding!!.rememIv.setImageResource(R.drawable.ic_uncheck)
                mCheck = false
            } else {
                binding!!.rememIv.setImageResource(R.drawable.ic_check)
                mCheck = true
            }
        })

        binding!!.txtTermsConditionsTV.setOnClickListener {
            val i = Intent(mActivity, WebViewActivity::class.java)
            i.putExtra(LINK_TYPE, LINK_TERMS)
            startActivity(i)
        }
    }


    fun setColorText() {
        val SpanString = SpannableString("Create An Account")
        SpanString.setSpan(ForegroundColorSpan(Color.parseColor("#0653a0")), 0, 6, 0)
        SpanString.setSpan(ForegroundColorSpan(Color.parseColor("#FF000000")), 7, 17, 0)
        binding?.createAnTV?.setText(SpanString, TextView.BufferType.SPANNABLE)
    }


    private fun mSignupParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = binding!!.firstNameET.text.toString()
        mMap["lastName"] = binding!!.lastNameET.text.toString()
        mMap["userName"] = binding!!.chooseET.text.toString()
        mMap["email"] = binding!!.emailET.text.toString()
        mMap["password"] = binding!!.passwordET.text.trim().toString()
        mMap["dob"] = binding!!.yourBirthTV.text.toString()
        mMap["deviceToken"] = mDeviceToken
        mMap["deviceType"] = DEVICE_TYPE
        mMap["orgID"] = mOrgId
        Log.e("SignupParams:", "$mMap")
        return mMap
    }

    private fun executeSignupApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.signupRequest(mSignupParams())
            .enqueue(object : Callback<UserDataModel> {
                override fun onFailure(call: Call<UserDataModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserDataModel>,
                    response: Response<UserDataModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            showPreviousActivityAlertDialog(mActivity, response.body()?.message)
                        }
                        400 -> {
                            val converter: Converter<ResponseBody, UserDataModel> =
                                RetrofitClient.retrofit?.responseBodyConverter(
                                    UserDataModel::class.java, arrayOfNulls<Annotation>(0)
                                )!!
                            val error: UserDataModel
                            try {
                                error = converter.convert(response.errorBody())!!
                                Log.e("error message", error.message!!)
                                showAlertDialog(mActivity, error.message)
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

            })

    }

    private fun mGoogleApiParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = binding!!.firstNameET.text.toString()
        mMap["lastName"] = binding!!.lastNameET.text.toString()
        mMap["userName"] = binding!!.chooseET.text.toString()
        mMap["email"] = binding!!.emailET.text.toString()
        mMap["googleToken"] = mGoogleUserId
        mMap["dob"] = binding!!.yourBirthTV.text.toString()
        mMap["googleImage"] = mGoogleProfileImage
        mMap["deviceToken"] = mDeviceToken
        mMap["deviceType"] = DEVICE_TYPE
        mMap["orgID"] = mOrgId
        Log.i("GoogleApiParams:", "$mMap")
        return mMap
    }

    private fun executeGoogleApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.googleSigninRequest(mGoogleApiParams())
            .enqueue(object : Callback<UserDataModel> {
                override fun onFailure(call: Call<UserDataModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserDataModel>,
                    response: Response<UserDataModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            AppPreference().writeBoolean(mActivity, IS_LOGIN, true)
                            AppPreference().writeString(
                                mActivity,
                                USERID,
                                response.body()?.data?.userID
                            )
                            AppPreference().writeString(
                                mActivity,
                                EMAIL,
                                response.body()?.data?.email
                            )
                            AppPreference().writeString(
                                mActivity,
                                FIRST_NAME,
                                response.body()?.data?.firstName
                            )
                            AppPreference().writeString(
                                mActivity,
                                LAST_NAME,
                                response.body()?.data?.lastName
                            )
                            AppPreference().writeString(
                                mActivity,
                                USERNAME,
                                response.body()?.data?.userName
                            )
                            AppPreference().writeString(
                                mActivity,
                                PROFILE_IMAGE,
                                response.body()?.data?.profileImage
                            )
                            AppPreference().writeString(
                                mActivity,
                                AUTH_TOKEN,
                                response.body()?.data?.authToken
                            )
                            AppPreference().writeString(
                                mActivity,
                                ORG_ID,
                                response.body()?.data?.orgID
                            )
                            AppPreference().writeString(
                                mActivity,
                                HOUR_TRACKER,
                                response.body()?.data?.hourTracker
                            )
                            AppPreference().writeString(mActivity, DOB, response.body()?.data?.dob)
                            showToast(mActivity, response.body()?.message)
                            startActivity(Intent(mActivity, MainActivity::class.java))
                            finish()
                            val preferences: SharedPreferences =
                                RememberMeAppPreference().getPreferences(applicationContext)
                            val editor = preferences.edit()
                            editor.clear()
                            editor.apply()
                            editor.commit()
                        }
                        400 -> {
                            val converter: Converter<ResponseBody, UserDataModel> =
                                RetrofitClient.retrofit?.responseBodyConverter(
                                    UserDataModel::class.java, arrayOfNulls<Annotation>(0)
                                )!!
                            val error: UserDataModel
                            try {
                                error = converter.convert(response.errorBody())!!
                                Log.e("error message", error.message!!)
                                showAlertDialog(mActivity, error.message)
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

            })
    }

    private fun mAppleApiParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = binding!!.firstNameET.text.toString()
        mMap["lastName"] = binding!!.lastNameET.text.toString()
        mMap["userName"] = binding!!.chooseET.text.toString()
        mMap["email"] = binding!!.emailET.text.toString()
        mMap["appleToken"] = mGoogleUserId
        mMap["dob"] = binding!!.yourBirthTV.text.toString()
        mMap["appleImage"] = mGoogleProfileImage
        mMap["deviceToken"] = mDeviceToken
        mMap["deviceType"] = DEVICE_TYPE
        mMap["orgID"] = mOrgId
        Log.i("AppleApiParams:", "$mMap")
        return mMap
    }

    private fun executeAppleApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.appleSigninRequest(mAppleApiParams())
            .enqueue(object : Callback<UserDataModel> {
                override fun onFailure(call: Call<UserDataModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserDataModel>,
                    response: Response<UserDataModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            AppPreference().writeBoolean(mActivity, IS_LOGIN, true)
                            AppPreference().writeString(
                                mActivity,
                                USERID,
                                response.body()?.data?.userID
                            )
                            AppPreference().writeString(
                                mActivity,
                                EMAIL,
                                response.body()?.data?.email
                            )
                            AppPreference().writeString(
                                mActivity,
                                FIRST_NAME,
                                response.body()?.data?.firstName
                            )
                            AppPreference().writeString(
                                mActivity,
                                LAST_NAME,
                                response.body()?.data?.lastName
                            )
                            AppPreference().writeString(
                                mActivity,
                                USERNAME,
                                response.body()?.data?.userName
                            )
                            AppPreference().writeString(
                                mActivity,
                                PROFILE_IMAGE,
                                response.body()?.data?.profileImage
                            )
                            AppPreference().writeString(
                                mActivity,
                                AUTH_TOKEN,
                                response.body()?.data?.authToken
                            )
                            AppPreference().writeString(
                                mActivity,
                                ORG_ID,
                                response.body()?.data?.orgID
                            )
                            AppPreference().writeString(
                                mActivity,
                                HOUR_TRACKER,
                                response.body()?.data?.hourTracker
                            )
                            AppPreference().writeString(mActivity, DOB, response.body()?.data?.dob)
                            showToast(mActivity, response.body()?.message)
                            startActivity(Intent(mActivity, MainActivity::class.java))
                            finish()
                            val preferences: SharedPreferences =
                                RememberMeAppPreference().getPreferences(applicationContext)
                            val editor = preferences.edit()
                            editor.clear()
                            editor.apply()
                            editor.commit()
                        }
                        400 -> {
                            val converter: Converter<ResponseBody, UserDataModel> =
                                RetrofitClient.retrofit?.responseBodyConverter(
                                    UserDataModel::class.java, arrayOfNulls<Annotation>(0)
                                )!!
                            val error: UserDataModel
                            try {
                                error = converter.convert(response.errorBody())!!
                                Log.e("error message", error.message!!)
                                showAlertDialog(mActivity, error.message)
                            } catch (e: IOException) {
                                e.printStackTrace()
                            }
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

            })
    }

    private fun mOrganisationApiParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["search"] = ""
        Log.e("OrganisationApiParams:", "$mMap")
        return mMap
    }

    // - - Execute Disease Api
    private fun executeOrganisationApi() {
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.getAllOrganisationForStudentRequest(mOrganisationApiParams())
            .enqueue(object : Callback<GetAllOrganisationModel> {
                override fun onResponse(
                    call: Call<GetAllOrganisationModel>,
                    response: Response<GetAllOrganisationModel>
                ) {
                    when (response.code()) {
                        200 -> {
                            mOrganisationList =
                                response.body()?.data as ArrayList<OrganisationDataItem?>?
                        }
                        401 -> {
                            showAuthFailedDialog(mActivity)
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

                override fun onFailure(call: Call<GetAllOrganisationModel>, t: Throwable) {
                }
            })
    }

    fun createRelationshipDialog(mActivity: Activity) {
        val mDataList: ArrayList<String> = ArrayList<String>()
        for (i in 0 until mOrganisationList?.size!!) {
            mDataList.add(mOrganisationList?.get(i)?.orgTitle!!)
        }
        val mBottomSheetDialog = BottomSheetDialog(this)
        @SuppressLint("InflateParams") val sheetView: View =
            mActivity.layoutInflater.inflate(R.layout.dialog_org, null)
        mBottomSheetDialog.setContentView(sheetView)
        mBottomSheetDialog.show()
        val relationshipWP: WheelPicker = sheetView.findViewById(R.id.relationshipWP)
        val txtCancelTV = sheetView.findViewById<TextView>(R.id.txtCancelTV)
        val txtSaveTV = sheetView.findViewById<TextView>(R.id.txtSaveTV)
        relationshipWP.setAtmospheric(true)
        relationshipWP.isCyclic = false
        relationshipWP.isCurved = true
        //Set Data
        relationshipWP.data = mDataList
        txtCancelTV.setOnClickListener { mBottomSheetDialog.dismiss() }

        txtSaveTV.setOnClickListener {
            if (mOrganisationList!!.size > 0) {
                mOrgId = "" + mOrganisationList!![relationshipWP.currentItemPosition]?.orgID
                mOrgName = mDataList.get(relationshipWP.currentItemPosition)
                binding!!.spinnerTV.text = mOrgName
                mBottomSheetDialog.dismiss()
            }
        }
    }

    /*
 * Set up validations for Sign In fields
 * */
    private fun isValidate(): Boolean {
        var flag = true
        if (binding?.firstNameET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_name))
            flag = false
        } else if (binding?.lastNameET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (binding?.chooseET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_choose_username))
            flag = false
        } else if (binding?.emailET?.getText().toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.getText().toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (binding?.passwordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_password))
            flag = false

        } else if (binding?.passwordET?.getText().toString().trim { it <= ' ' }.length < 6) {
            showAlertDialog(mActivity, getString(R.string.please_enter_password_minimum_8_digit))
            flag = false

        } else if (binding?.confirmPasswordET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_con_password))
            flag = false

        } else if (!binding?.passwordET?.getText().toString()
                .equals(binding?.confirmPasswordET?.getText().toString())
        ) {
            showAlertDialog(mActivity, getString(R.string.match_password_signup))
            flag = false
        } else if (binding?.spinnerTV?.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_select_organisation))
            flag = false
        } else if (binding?.yourBirthTV?.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_birth_date))
            flag = false
        } else if (!mCheck) {
            showAlertDialog(mActivity, getString(R.string.please_agrre_with_terms))
            flag = false
        }
        return flag
    }

    private fun isGoogleValidate(): Boolean {
        var flag = true
        if (binding?.firstNameET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_name))
            flag = false
        } else if (binding?.lastNameET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
            flag = false
        } else if (binding?.chooseET?.getText().toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_choose_username))
            flag = false
        } else if (binding?.emailET?.getText().toString().trim() == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_email_address))
            flag = false
        } else if (!isValidEmaillId(binding?.emailET?.getText().toString())) {
            showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
            flag = false
        } else if (binding?.yourBirthTV?.text.toString().trim { it <= ' ' } == "") {
            showAlertDialog(mActivity, getString(R.string.please_enter_birth_date))
            flag = false
        } else if (!mCheck) {
            showAlertDialog(mActivity, getString(R.string.please_agrre_with_terms))
            flag = false
        }
        return flag
    }

    override fun onBackPressed() {
        super.onBackPressed()
        startActivity(Intent(mActivity, WelcomeActivity::class.java))
        finish()
    }
}