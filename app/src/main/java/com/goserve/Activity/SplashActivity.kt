package com.goserve.Activity

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import com.goserve.Activity.Fragment.NotificationFragment
import com.goserve.R
import com.goserve.databinding.ActivitySplashBinding
import com.goserve.utils.*
import com.google.gson.JsonObject
import org.json.JSONObject
import org.json.JSONException










class SplashActivity : BasicActivity() {

    /*** Getting the Current Class Name */
    override var TAG = this@SplashActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@SplashActivity
    var binding: ActivitySplashBinding? = null
    var mOrganisationTitle = ""
    var mOrganisationDetail = ""
    var mOrganisationImage = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)
        setUpSplash()
    }

    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())
                if (isLogin()) {
                    if (intent != null) {
                        if (intent.getStringExtra("mType") != null && intent.getStringExtra("mType").equals("2")) {
                            if (intent.getStringExtra("isApprove") != null && intent.getStringExtra("isApprove").equals("1")) {
                                mOrganisationTitle = intent.getStringExtra("orgTitle")!!
                                mOrganisationDetail = intent.getStringExtra("orgDetail")!!
                                mOrganisationImage = intent.getStringExtra("orgImage")!!
                                AppPreference().writeString(mActivity, ORG_TITLE, mOrganisationTitle)
                                AppPreference().writeString(mActivity, ORG_DETAIL, mOrganisationDetail)
                                AppPreference().writeString(mActivity, ORG_IMAGE, mOrganisationImage)
                                val i = Intent(mActivity, OrganisationDetailActivity::class.java)
                                i.putExtra("Push", "push")
                                i.putExtra("orgTitle",mOrganisationTitle)
                                i.putExtra("orgDetail",mOrganisationDetail)
                                i.putExtra("orgImage",mOrganisationImage)
                                startActivity(i)
                                finish()
                            } else if (intent.getStringExtra("isApprove") != null && intent.getStringExtra(
                                    "isApprove"
                                ).equals("2")
                            ) {
                                mOrganisationTitle = intent.getStringExtra("orgTitle")!!
                                mOrganisationDetail = intent.getStringExtra("orgDetail")!!
                                mOrganisationImage = intent.getStringExtra("orgImage")!!
                                AppPreference().writeString(
                                    mActivity,
                                    ORG_TITLE,
                                    mOrganisationTitle
                                )
                                AppPreference().writeString(
                                    mActivity,
                                    ORG_DETAIL,
                                    mOrganisationDetail
                                )
                                AppPreference().writeString(
                                    mActivity,
                                    ORG_IMAGE,
                                    mOrganisationImage
                                )
                                val i = Intent(mActivity, OrganisationDetailActivity::class.java)
                                i.putExtra("Push", "push")
                                startActivity(i)
                                finish()
                            }
                        } else if (intent.getStringExtra("mType") != null && intent.getStringExtra("mType")
                                .equals("4")
                        ) {
                            val i = Intent(mActivity, MainActivity::class.java)
                            i.putExtra("Push", "push")
                            startActivity(i)
                            finish()
                        } else if (intent.getStringExtra("mType") != null && intent.getStringExtra("mType")
                                .equals("5"))
                                {
                            val i = Intent(mActivity, MainActivity::class.java)
                            i.putExtra("Push", "push")
                            startActivity(i)
                            finish()
                        } else if (intent.getStringExtra("mType") != null && intent.getStringExtra("mType").equals("6")) {
                            var mJsonObject = JSONObject(intent.getStringExtra("mOpDetailsJsonObject"))
                            val i = Intent(mActivity, OpportunityDetailActivity::class.java)
                            i.putExtra("Push", "push")
                            i.putExtra("mOpDetailsJsonObject",mJsonObject.toString())
                            startActivity(i)
                            finish()
                        }
                        else if (intent.getStringExtra("mType") != null && intent.getStringExtra("mType").equals("12")) {
                            var mJsonObject = JSONObject(intent.getStringExtra("mOpDetailsJsonObject"))
                            val i = Intent(mActivity, OpportunityDetailActivity::class.java)
                            i.putExtra("Push", "push")
                            i.putExtra("mOpDetailsJsonObject",mJsonObject.toString())
                            startActivity(i)
                            finish()
                        }
                        else if (intent.getStringExtra("mType") != null && intent.getStringExtra("mType").equals("13")) {
                            var mJsonObject = JSONObject(intent.getStringExtra("mOpDetailsJsonObject"))
                            val i = Intent(mActivity, OpportunityDetailActivity::class.java)
                            i.putExtra("Push", "push")
                            i.putExtra("mOpDetailsJsonObject",mJsonObject.toString())
                            startActivity(i)
                            finish()
                        }
                        else if (intent.getStringExtra("mType") != null && intent.getStringExtra("mType").equals("14")) {
                            var mJsonObject = JSONObject(intent.getStringExtra("mOpDetailsJsonObject"))
                            val i = Intent(mActivity, OpportunityDetailActivity::class.java)
                            i.putExtra("Push", "push")
                            i.putExtra("mOpDetailsJsonObject",mJsonObject.toString())
                            startActivity(i)
                            finish()
                        }
                        else{
                            val i = Intent(mActivity, MainActivity::class.java)
                            startActivity(i)
                            finish()

                        }
                    }
                }

                else {
                    if (intent != null) {
                        if (intent.getStringExtra("Push") != null && intent.getStringExtra("Push")
                                .equals("push")
                        ) {
                            val i = Intent(mActivity, LoginActivity::class.java)
                            startActivity(i)
                            finish()
                        } else {
                            val i = Intent(mActivity, WelcomeActivity::class.java)
                            startActivity(i)
                            finish()
                        }

                    }
                }
            }
        }
        mThread.start()
    }
}