package com.goserve.Activity

import android.os.Bundle
import android.webkit.WebViewClient
import com.goserve.R
import com.goserve.utils.*
import kotlinx.android.synthetic.main.activity_web_view.*

class WebViewActivity : BasicActivity() {

    // - - Initialize Objects
    var linkType: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_web_view)
        getIntentData()
        backRL.setOnClickListener {
            onBackPressed()
        }
    }

    // - - Get Intent Data from Another Activity
    private fun getIntentData() {
        if (intent != null) {
            linkType = intent.getStringExtra(LINK_TYPE).toString()
            when (linkType) {
                LINK_ABOUT -> {
                    txtHeadingTV.text = getString(R.string.about_us)
                    setUpWebView(ABOUT_WEB_LINK)
                }
                LINK_TERMS -> {
                    txtHeadingTV.text = getString(R.string.term_of_use)
                    setUpWebView(TERMS_WEB_LINK)
                }
                LINK_PP -> {
                    txtHeadingTV.text = getString(R.string.privacy_policy)
                    setUpWebView(PP_WEB_LINK)
                }
            }
        }
    }

    // - - Opens WebView in Application
    private fun setUpWebView(mUrl: String) {
        // this will set up webView client
        linkWV.webViewClient = WebViewClient()
        // this will load the url of the website
        linkWV.loadUrl(mUrl)
        // this will enable the javascript settings
        linkWV.settings.javaScriptEnabled = true
        // if you want to enable zoom feature
        linkWV.settings.setSupportZoom(true)
    }

}