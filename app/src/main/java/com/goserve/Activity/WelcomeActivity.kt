package com.goserve.Activity

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.Scopes
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.Scope
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.OAuthProvider
import com.google.firebase.messaging.FirebaseMessaging
import com.goserve.R
import com.goserve.databinding.ActivityWelcomeBinding
import com.goserve.model.UserDataModel
import com.goserve.utils.*
import com.playerpollmyapp.retrofit.ApiInterface
import com.playerpollmyapp.retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.security.SecureRandom
import java.util.HashMap

class WelcomeActivity : BasicActivity() {

    /*** Getting the Current Class Name */
    override var TAG = this@WelcomeActivity.javaClass.simpleName

    /** * Current Activity Instance */
    override var mActivity: Activity = this@WelcomeActivity
    private var mDeviceToken: String = ""
    private lateinit var mGoogleSignInClient: GoogleSignInClient
    private lateinit var mFirebaseAuth: FirebaseAuth
    var binding: ActivityWelcomeBinding? = null
    val provider = OAuthProvider.newBuilder("apple.com")
    private var mGoogleFirstName: String = ""
    private var mGoogleLastName: String = ""
    private var mType: String = ""
    private var mLoginType:String=""
    private var mGoogleUserId: String = ""
    private var mGoogleEmail: String = ""
    private var mGoogleProfileImage: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWelcomeBinding.inflate(layoutInflater)
        setContentView(binding!!.root)
        setStatusBar(mActivity, Color.WHITE)
        provider.setScopes(arrayOf("email", "name").toMutableList())
        initializeGoogle()
        performClick()
        getDeviceToken()
    }

    // - - To Get Device Token
    private fun getDeviceToken() {
        FirebaseApp.initializeApp(this)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.e(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            mDeviceToken = task.result!!
            Log.e("DeviceToken:", "DeviceToken:$mDeviceToken")
        })
    }

    fun performClick() {

        binding?.txtSignInTV?.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        })

        binding?.signUpWithEmailTV?.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, SignUpActivity::class.java)
            startActivity(intent)
            finish()
        })

        binding!!.googleRL.setOnClickListener {
            mType = "1"
            mLoginType="google"
            performGoogleClick()
        }
        binding!!.appleRL.setOnClickListener {
            mType = "2"
            mLoginType="apple"
            performAppleClick()
        }
    }

    private fun performAppleClick() {
        mFirebaseAuth?.startActivityForSignInWithProvider(this, provider.build())
            ?.addOnSuccessListener { authResult ->
                val name = authResult.user!!.displayName.toString()
                mGoogleEmail = authResult.user!!.email.toString()
                mGoogleFirstName = name
                mGoogleLastName = name
                mGoogleUserId = authResult.user!!.uid
                executeGoogleAppleTokenApi()
            }
            ?.addOnFailureListener { e ->
                Log.w("", "activitySignIn:onFailure", e)
            }
    }

    private fun performGoogleClick() {
        val signInIntent: Intent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            mGoogleUserId = account.id.toString()
            Log.e("Token", account.idToken.toString())
            val name = account.displayName
            Log.e("ServerAuthCode", account.serverAuthCode.toString())
            mGoogleEmail = account.email.toString()
            mGoogleProfileImage = if (account.photoUrl != null) {
                account.photoUrl.toString()
            } else {
                ""
            }
            val idx = name!!.lastIndexOf(' ')
            if (idx == -1) {
                return
            }
            mGoogleFirstName = name.substring(0, idx)
            mGoogleLastName = name.substring(idx + 1)
            Log.i("mGoogleUserId", mGoogleUserId)
            executeGoogleAppleTokenApi()
        } catch (e: ApiException) {
            Log.e("MyTAG", "signInResult:failed code=" + e.statusCode)
            showToast(mActivity, "Failed")
        }
    }

    private fun mGoogleAppleTokenApiParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["type"] = mType
        mMap["token"] = mGoogleUserId
        Log.e("GoogleAppleTokenParams:", "$mMap")
        return mMap
    }

    private fun executeGoogleAppleTokenApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface = RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.checkGoogleAppleTokenRequest(mGoogleAppleTokenApiParams())
            .enqueue(object : Callback<UserDataModel> {
                override fun onFailure(call: Call<UserDataModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserDataModel>,
                    response: Response<UserDataModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            AppPreference().writeString(mActivity, USERID, response.body()?.data?.userID)
                            AppPreference().writeString(mActivity, EMAIL, response.body()?.data?.email)
                            AppPreference().writeString(mActivity, FIRST_NAME, response.body()?.data?.firstName)
                            AppPreference().writeString(mActivity, LAST_NAME, response.body()?.data?.lastName)
                            AppPreference().writeString(mActivity, USERNAME, response.body()?.data?.userName)
                            AppPreference().writeString(mActivity, DOB, response.body()?.data?.dob)
                            AppPreference().writeString(mActivity, PROFILE_IMAGE, response.body()?.data?.profileImage)
                            AppPreference().writeString(mActivity, ORG_ID, response.body()?.data?.orgID)
                            if(mType=="1"){
                                executeGoogleApi()
                            }else if(mType=="2"){
                                executeAppleApi()
                            }
                        }
                        400 -> {
                            val intent = Intent(mActivity, SignUpActivity::class.java)
                            intent.putExtra("Type", mLoginType)
                            intent.putExtra("mGoogleUserId", mGoogleUserId)
                            intent.putExtra("mGoogleEmail", mGoogleEmail)
                            intent.putExtra("mGoogleFirstName", mGoogleFirstName)
                            intent.putExtra("mGoogleLastName", mGoogleLastName)
                            intent.putExtra("mGoogleProfileImage", mGoogleProfileImage)
                            startActivity(intent)
                            finish()
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

            })

    }

    private fun mGoogleApiParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = firstName()
        mMap["lastName"] = lastName()
        mMap["userName"] = userName()
        mMap["email"] = userEmail()
        mMap["googleToken"] = mGoogleUserId
        mMap["googleImage"] = mGoogleProfileImage
        mMap["dob"] = dob()
        mMap["deviceToken"] = mDeviceToken
        mMap["deviceType"] = DEVICE_TYPE
        mMap["orgID"] = orgId()
        Log.e("GoogleApiParams:", "$mMap")
        return mMap
    }

    private fun executeGoogleApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.googleSigninRequest(mGoogleApiParams())
            .enqueue(object : Callback<UserDataModel> {
                override fun onFailure(call: Call<UserDataModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserDataModel>,
                    response: Response<UserDataModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            AppPreference().writeBoolean(mActivity, IS_LOGIN, true)
                            AppPreference().writeString(mActivity, USERID, response.body()?.data?.userID)
                            AppPreference().writeString(mActivity, EMAIL, response.body()?.data?.email)
                            AppPreference().writeString(mActivity, FIRST_NAME, response.body()?.data?.firstName)
                            AppPreference().writeString(mActivity, LAST_NAME, response.body()?.data?.lastName)
                            AppPreference().writeString(mActivity, USERNAME, response.body()?.data?.userName)
                            AppPreference().writeString(mActivity, PROFILE_IMAGE, response.body()?.data?.profileImage)
                            AppPreference().writeString(mActivity, AUTH_TOKEN, response.body()?.data?.authToken)
                            AppPreference().writeString(mActivity, DOB, response.body()?.data?.dob)
                            AppPreference().writeString(mActivity, ORG_ID, response.body()?.data?.orgID)
                            AppPreference().writeString(mActivity, HOUR_TRACKER, response.body()?.data?.hourTracker)
                            showToast(mActivity, response.body()?.message)
                            startActivity(Intent(mActivity, MainActivity::class.java))
                            finish()
                            val preferences: SharedPreferences = RememberMeAppPreference().getPreferences(applicationContext)
                            val editor = preferences.edit()
                            editor.clear()
                            editor.apply()
                            editor.commit()
                        }
                        403 -> {
                            showAccountDisabledDialog(mActivity)
                        }
                        400 -> {
                            showAlertDialog(mActivity, getString(R.string.email_username_is_already_registered))
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

            })

    }

    private fun mAppleApiParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["firstName"] = firstName()
        mMap["lastName"] = lastName()
        mMap["userName"] = userName()
        mMap["email"] = userEmail()
        mMap["appleToken"] = mGoogleUserId
        mMap["appleImage"] = mGoogleProfileImage
        mMap["dob"] = dob()
        mMap["deviceToken"] = mDeviceToken
        mMap["deviceType"] = DEVICE_TYPE
        mMap["orgID"] = orgId()
        Log.i("AppleApiParams:", "$mMap")
        return mMap
    }

    private fun executeAppleApi() {
        showProgressDialog(mActivity)
        val mApiInterface: ApiInterface =
            RetrofitClient.getApiClient()?.create(ApiInterface::class.java)!!
        mApiInterface.appleSigninRequest(mAppleApiParams())
            .enqueue(object : Callback<UserDataModel> {
                override fun onFailure(call: Call<UserDataModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<UserDataModel>,
                    response: Response<UserDataModel>
                ) {
                    dismissProgressDialog()
                    when (response.code()) {
                        200 -> {
                            AppPreference().writeBoolean(mActivity, IS_LOGIN, true)
                            AppPreference().writeString(mActivity, USERID, response.body()?.data?.userID)
                            AppPreference().writeString(mActivity, EMAIL, response.body()?.data?.email)
                            AppPreference().writeString(mActivity, FIRST_NAME, response.body()?.data?.firstName)
                            AppPreference().writeString(mActivity, LAST_NAME, response.body()?.data?.lastName)
                            AppPreference().writeString(mActivity, USERNAME, response.body()?.data?.userName)
                            AppPreference().writeString(mActivity, PROFILE_IMAGE, response.body()?.data?.profileImage)
                            AppPreference().writeString(mActivity, AUTH_TOKEN, response.body()?.data?.authToken)
                            AppPreference().writeString(mActivity, ORG_ID, response.body()?.data?.orgID)
                            AppPreference().writeString(mActivity, HOUR_TRACKER, response.body()?.data?.hourTracker)
                            AppPreference().writeString(mActivity, DOB, response.body()?.data?.dob)
                            showToast(mActivity, response.body()?.message)
                            startActivity(Intent(mActivity, MainActivity::class.java))
                            finish()
                            val preferences: SharedPreferences = RememberMeAppPreference().getPreferences(applicationContext)
                            val editor = preferences.edit()
                            editor.clear()
                            editor.apply()
                            editor.commit()
                        }
                        403 -> {
                            showAccountDisabledDialog(mActivity)
                        }
                        400 -> {
                            showAlertDialog(
                                mActivity,
                                getString(R.string.email_username_is_already_registered)
                            )
                        }
                        500 -> {
                            showToast(mActivity, getString(R.string.internal_server_error))
                        }
                    }
                }

            })
    }

    private fun initializeGoogle() {
        FirebaseApp.initializeApp(this)
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mFirebaseAuth = FirebaseAuth.getInstance()
        FirebaseAuth.getInstance().signOut()
        signOut()
    }

    private fun signOut() {
        mGoogleSignInClient!!.signOut()
            .addOnCompleteListener(this) {
                Log.e(TAG, "==Logout Successfully==")
            }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            GOOGLE_SIGN_IN -> {
                val completedTask: Task<GoogleSignInAccount> =
                    GoogleSignIn.getSignedInAccountFromIntent(data)
                handleSignInResult(completedTask)
            }
        }
    }
}