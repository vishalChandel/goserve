package com.goserve.Adapter


import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.browser.customtabs.CustomTabColorSchemeParams
import androidx.browser.customtabs.CustomTabsIntent
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.goserve.Activity.OpportunityDetailActivity
import com.goserve.Interfaces.InterfaceBottomSheet
import com.goserve.Interfaces.LoadMoreOpportunities
import com.goserve.Interfaces.OpportunityInterface
import com.goserve.R
import com.goserve.model.OpportunitiesDataItem
import com.goserve.utils.PACKAGE_NAME
import java.io.Serializable


class ALLAdapter(
    val mActivity: Activity?,
    var mAllOpportunitiesList: ArrayList<OpportunitiesDataItem?>?,
    val InterfaceBottomSheet: InterfaceBottomSheet,
    val s: String,
    var mLoadMore: LoadMoreOpportunities?,
    var mOpportunityInterface: OpportunityInterface,
) :
    RecyclerView.Adapter<ALLAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_all_fragment, null)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel = mAllOpportunitiesList!![position]
        val opportunitiesList = mAllOpportunitiesList
        if (mAllOpportunitiesList!![position]?.isAds == "1") {
            holder.TimeTV.visibility=View.GONE
            holder.dotIvRL.visibility=View.GONE
            holder.tittleTV.text = mAllOpportunitiesList!![position]?.title
            holder.subTittleTV.text = mAllOpportunitiesList!![position]?.description
            mActivity?.let {
                Glide.with(it).load(mAllOpportunitiesList!![position]?.opImage)
                    .placeholder(R.drawable.img_app_placeholder)
                    .error(R.drawable.img_app_placeholder)
                    .into(holder.imgIV)
            }
            holder.itemView.setOnClickListener {
                    val builder = CustomTabsIntent.Builder()
                    // to set the toolbar color use CustomTabColorSchemeParams
                    // since CustomTabsIntent.Builder().setToolBarColor() is deprecated
                    val params = CustomTabColorSchemeParams.Builder()
                    params.setToolbarColor(ContextCompat.getColor(mActivity!!, R.color.colorLightBlack))
                    builder.setDefaultColorSchemeParams(params.build())
                    // shows the title of web-page in toolbar
                    builder.setShowTitle(true)
                    // setShareState(CustomTabsIntent.SHARE_STATE_ON) will add a menu to share the web-page
                    builder.setShareState(CustomTabsIntent.SHARE_STATE_ON)
                    // to set weather instant apps is enabled for the custom tab or not, use
                    builder.setInstantAppsEnabled(true)
                    val customBuilder = builder.build()
                    openCustomTab(mActivity, builder.build(), Uri.parse(mAllOpportunitiesList!![position]?.link))
                }

        } else {
        holder.TimeTV.text = mAllOpportunitiesList!![position]?.opHour
        holder.tittleTV.text = mAllOpportunitiesList!![position]?.title
        holder.subTittleTV.text = mAllOpportunitiesList!![position]?.description
        mActivity?.let {
            Glide.with(it).load(mAllOpportunitiesList!![position]?.opImage)
                .placeholder(R.drawable.img_app_placeholder)
                .error(R.drawable.img_app_placeholder)
                .into(holder.imgIV)
        }

        holder.dotIvRL.setOnClickListener(View.OnClickListener {
            mOpportunityInterface.mInterface(mModel, position)
            InterfaceBottomSheet?.onClickBottomSheet(position, mActivity)
        })
            holder.itemView.setOnClickListener {
                if (s == "Serving") {
                    val intent = Intent(mActivity, OpportunityDetailActivity::class.java)
                    intent.putExtra("Tag", "Serving")
                    intent.putExtra("pos", position.toString())
                    intent.putExtra("opportunitiesData", opportunitiesList as Serializable?)
                    mActivity?.startActivity(intent)
                } else if (s == "All") {
                    val intent = Intent(mActivity, OpportunityDetailActivity::class.java)
                    intent.putExtra("Tag", "All")
                    intent.putExtra("opportunitiesData", opportunitiesList as Serializable?)
                    intent.putExtra("pos", position.toString())
                    mActivity?.startActivity(intent)
                }
            }
        }

        if (mAllOpportunitiesList!!.size % 20 == 0 && mAllOpportunitiesList!!.size - 1 == position) {
            mLoadMore?.mLoadMoreOpportunities(mAllOpportunitiesList!![position]!!)
        }
    }

    override fun getItemCount(): Int {
        return mAllOpportunitiesList!!.size
    }

    open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tittleTV: TextView
        var TimeTV: TextView
        var imgIV: ImageView
        var dotIV: ImageView
        var dotIvRL: RelativeLayout
        var subTittleTV: TextView

        init {
            imgIV = itemView.findViewById<View>(R.id.imgIV) as ImageView
            dotIV = itemView.findViewById<View>(R.id.dotIV) as ImageView
            tittleTV = itemView.findViewById<View>(R.id.tittleTV) as TextView
            TimeTV = itemView.findViewById<View>(R.id.TimeTV) as TextView
            dotIvRL = itemView.findViewById<View>(R.id.dotIvRL) as RelativeLayout
            subTittleTV = itemView.findViewById<View>(R.id.subTittleTV) as TextView
        }
    }


    fun openCustomTab(activity: Activity, customTabsIntent: CustomTabsIntent, uri: Uri?) {
        // package name is the default package
        // for our custom chrome tab
        val packageName = "com.android.chrome"

        // we are checking if the package name is not null
        // if package name is not null then we are calling
        // that custom chrome tab with intent by passing its
        // package name.
        customTabsIntent.intent.setPackage(packageName)

        // in that custom tab intent we are passing
        // our url which we have to browse.
        customTabsIntent.launchUrl(activity, uri!!)
    }
}
