package com.goserve.Adapter

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.goserve.Activity.OpportunityDetailActivity
import com.goserve.Activity.OrganisationDetailActivity
import com.goserve.R
import com.goserve.model.NotificationDataItem
import java.io.Serializable
import java.text.SimpleDateFormat
import java.util.*

class NotificationAdapter(
    val mActivity: Activity?,
    var mNotificationList: ArrayList<NotificationDataItem?>?
) :
    RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_notification, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
     if (mNotificationList!![position]?.notificationType == "2") {
            holder.txtNotificationTitleTV.visibility = View.VISIBLE
            holder.notificationTV.text = mNotificationList!![position]?.description
            holder.dateTimeTV.text =
                convertLongToStringDate(mNotificationList!![position]?.created.toString())
            holder.txtNotificationTitleTV.text = mNotificationList!![position]?.title
            holder.notificationTV.text = mNotificationList!![position]?.description
            mActivity?.let {
                Glide.with(it).load(mNotificationList!![position]?.organizationDetails?.orgImage)
                    .placeholder(R.mipmap.app_icon)
                    .error(R.mipmap.app_icon)
                    .into(holder.imgIV)
            }
        } else {
            holder.txtNotificationTitleTV.visibility = View.VISIBLE
            holder.txtNotificationTitleTV.text = mNotificationList!![position]?.title
            holder.notificationTV.text = mNotificationList!![position]?.description
            holder.dateTimeTV.text =
                convertLongToStringDate(mNotificationList!![position]?.created.toString())
            mActivity?.let {
                Glide.with(it).load(mNotificationList!![position]?.opportunityDetails?.opImage)
                    .placeholder(R.mipmap.app_icon)
                    .error(R.mipmap.app_icon)
                    .into(holder.imgIV)
            }
        }

        holder.itemView.setOnClickListener {
            if (mNotificationList!![position]?.notificationType == "2") {
                val i=Intent(mActivity,OrganisationDetailActivity::class.java)
                i.putExtra("Push","push")
                i.putExtra("orgTitle",mNotificationList!![position]?.organizationDetails?.orgTitle)
                i.putExtra("orgDetail",mNotificationList!![position]?.organizationDetails?.orgDescription)
                i.putExtra("orgImage",mNotificationList!![position]?.organizationDetails?.orgImage)
                mActivity!!.startActivity(i)
            }
            else if(mNotificationList!![position]?.notificationType == "6"){
                val intent=Intent(mActivity, OpportunityDetailActivity::class.java)
                intent.putExtra("Tag","Notification")
                intent.putExtra("opportunitiesNData", mNotificationList as Serializable?)
                intent.putExtra("pos",position.toString())
                mActivity?.startActivity(intent)
            }
            else if(mNotificationList!![position]?.notificationType == "12"){
                val intent=Intent(mActivity, OpportunityDetailActivity::class.java)
                intent.putExtra("Tag","Notification")
                intent.putExtra("opportunitiesNData", mNotificationList as Serializable?)
                intent.putExtra("pos",position.toString())
                mActivity?.startActivity(intent)
            }
            else if(mNotificationList!![position]?.notificationType == "13"){
                val intent=Intent(mActivity, OpportunityDetailActivity::class.java)
                intent.putExtra("Tag","Notification")
                intent.putExtra("opportunitiesNData", mNotificationList as Serializable?)
                intent.putExtra("pos",position.toString())
                mActivity?.startActivity(intent)
            }
            else if(mNotificationList!![position]?.notificationType == "14"){
                val intent=Intent(mActivity, OpportunityDetailActivity::class.java)
                intent.putExtra("Tag","Notification")
                intent.putExtra("opportunitiesNData", mNotificationList as Serializable?)
                intent.putExtra("pos",position.toString())
                mActivity?.startActivity(intent)
            }
        }
    }


    override fun getItemCount(): Int {
        return mNotificationList!!.size
    }

    open class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dateTimeTV: TextView
        var notificationTV: TextView
        var txtNotificationTitleTV: TextView
        var imgIV: ImageView

        init {
            dateTimeTV = itemView.findViewById(R.id.dateTimeTV)
            notificationTV = itemView.findViewById(R.id.notificationTV)
            txtNotificationTitleTV = itemView.findViewById(R.id.txtNotificationTitleTV)
            imgIV = itemView.findViewById(R.id.notificationIV)
        }
    }

    // - - Get Date/Time from TimeStamp
    private fun convertLongToStringDate(mLong: String): String {
        val d = Date(mLong.toLong() * 1000).toString()
        val formatter3 = SimpleDateFormat("EE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH)
        var value = formatter3.parse(d)
        val dateFormatter = SimpleDateFormat("MMMM dd yyyy, HH:mm")
        dateFormatter.timeZone = TimeZone.getDefault()
        var ourDate: String = dateFormatter.format(value)
        return ourDate

    }
}