package com.goserve.Adapter

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.goserve.Activity.Fragment.UpFragment
import com.goserve.Activity.Fragment.OpporFragment


class Pager_Adapter(fm: FragmentManager?) :
    FragmentPagerAdapter(fm!!) {
    private val mFragmentList: MutableList<Fragment> = ArrayList()


    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> UpFragment()
            1 -> OpporFragment()
            else -> UpFragment()
        }
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFrag(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
    }


}

