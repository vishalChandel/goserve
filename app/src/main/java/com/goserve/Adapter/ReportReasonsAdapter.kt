package com.goserve.Adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.goserve.Interfaces.ReportReasonInterface
import com.goserve.R
import com.goserve.model.ReportReasonDataItem

class ReportReasonsAdapter(
    var context: Activity?,
    var mReportReasonsArrayList: ArrayList<ReportReasonDataItem?>?,
    var mReportReasonsInterface: ReportReasonInterface
) :
    RecyclerView.Adapter<ReportReasonsAdapter.MyViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_report_reasons, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val mModel = mReportReasonsArrayList!![position]
        holder.txtReasonTV.text=mModel?.reportReasons
        holder.itemView.setOnClickListener {
            mReportReasonsInterface.mInterface(mModel, position)
        }

    }

    override fun getItemCount(): Int {
        return mReportReasonsArrayList!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtReasonTV: TextView = itemView.findViewById(R.id.txtReasonTV)
    }
}