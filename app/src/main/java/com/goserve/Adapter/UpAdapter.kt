package com.goserve.Adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.goserve.Activity.OpportunityDetailActivity
import com.goserve.Interfaces.LoadMorePastOp
import com.goserve.R
import com.goserve.model.OpportunitiesDataItem
import com.goserve.model.PastOpDataItem
import java.io.Serializable
import java.util.ArrayList

class UpAdapter(
    val mActivity: Activity?,
    var mPastOpList: ArrayList<OpportunitiesDataItem?>?,
    var mLoadMore: LoadMorePastOp?
) :
        RecyclerView.Adapter<UpAdapter.ViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            val view: View = LayoutInflater.from(mActivity).inflate(R.layout.item_up, null)
            return ViewHolder(view)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.tittleTV.text=mPastOpList!![position]?.title
            holder.subTittleTV.text=mPastOpList!![position]?.description
            mActivity?.let {
                Glide.with(it).load(mPastOpList!![position]?.opImage)
                    .placeholder(R.drawable.img_app_placeholder)
                    .error(R.drawable.img_app_placeholder)
                    .into(holder.imgIV)
            }
            if (mPastOpList!!.size % 20 == 0 && mPastOpList!!.size - 1 == position) {
                mLoadMore?.mLoadMorePastOp(mPastOpList!![position]!!)
            }

            holder.itemView.setOnClickListener {
                    val intent= Intent(mActivity, OpportunityDetailActivity::class.java)
                    intent.putExtra("Tag","AccountTab")
                    intent.putExtra("pos",position.toString())
                    intent.putExtra("opportunitiesData", mPastOpList as Serializable?)
                    mActivity?.startActivity(intent)
                }
        }

        override fun getItemCount(): Int {
            return mPastOpList!!.size
        }

        open inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tittleTV: TextView
            var imgIV: ImageView
            var subTittleTV:TextView

            init {
                imgIV = itemView.findViewById<View>(R.id.imgIV) as ImageView
                tittleTV = itemView.findViewById<View>(R.id.tittleTV) as TextView
                subTittleTV = itemView.findViewById<View>(R.id.subTittleTV) as TextView
            }
        }
    }
