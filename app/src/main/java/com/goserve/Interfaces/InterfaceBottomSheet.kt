package com.goserve.Interfaces

import android.app.Activity

public interface InterfaceBottomSheet {

    fun onClickBottomSheet(position: Int, context: Activity?)

}