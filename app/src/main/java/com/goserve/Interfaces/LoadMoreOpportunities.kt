package com.goserve.Interfaces

import com.goserve.model.OpportunitiesDataItem

interface LoadMoreOpportunities {
    public fun mLoadMoreOpportunities(mModel: OpportunitiesDataItem)
}