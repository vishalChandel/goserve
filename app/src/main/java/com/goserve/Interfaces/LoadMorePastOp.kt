package com.goserve.Interfaces

import com.goserve.model.OpportunitiesDataItem
import com.goserve.model.PastOpDataItem

interface LoadMorePastOp {
    public fun mLoadMorePastOp(mModel: OpportunitiesDataItem)
}