package com.goserve.fcm

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.goserve.Activity.OpportunityDetailActivity
import com.goserve.Activity.OrganisationDetailActivity
import com.goserve.Activity.SplashActivity
import com.goserve.R
import org.json.JSONObject


class FirebaseMessagingService : FirebaseMessagingService() {

    val TAG: String = "NotificationReciever"
    var NOTIFICATION_ID = 1
    var mTitle: String? = ""
    var description = ""
    var mRoomID = ""
    var userID = ""
    var otherUserID = ""
    var mPollId = ""
    var created = ""
    var mType = ""
    var message: String? = ""
    var userName = ""
    var mProfileImage = " "
    var mOrganisationTitle = ""
    var mOrganisationDetail = ""
    var mOrganisationImage = ""
    var mIsApprove = ""
    var mOpportunityJsonObject: JSONObject? = null

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        if (p0.data.size > 0) {
            try {
                var mTempData = p0.data.get("notificationData")
                val mJsonObject: JSONObject = JSONObject(mTempData)

                if (!mJsonObject.isNull("notification_type")) {
                    mType = mJsonObject.getString("notification_type")
                }
                if (!mJsonObject.isNull("title")) {
                    userName = mJsonObject.getString("title")
                }
                if (!mJsonObject.isNull("roomID")) {
                    mRoomID = mJsonObject.getString("roomID")
                }
                if (!mJsonObject.isNull("userID")) {
                    userID = mJsonObject.getString("userID")
                }
                if (!mJsonObject.isNull("otherUserID")) {
                    otherUserID = mJsonObject.getString("otherUserID")
                }
                if (!mJsonObject.isNull("detailsID")) {
                    mPollId = mJsonObject.getString("detailsID")
                }
                if (!mJsonObject.isNull("description")) {
                    description = mJsonObject.getString("description")
                }
                if (!mJsonObject.isNull("created")) {
                    created = mJsonObject.getString("created")
                }
                if (!mJsonObject.isNull("profileImage")) {
                    mProfileImage = mJsonObject.getString("profileImage")
                }
                if (!mJsonObject.isNull("isApprove")) {
                    mIsApprove = mJsonObject.getString("isApprove")
                }
                //  Get Organization Details
                if (mType == "2") {
                    val mOrganizationJsonObject: JSONObject = mJsonObject.getJSONObject("organizationDetails")
                    if (!mOrganizationJsonObject.isNull("orgTitle")) {
                        mOrganisationTitle = mOrganizationJsonObject.getString("orgTitle")
                    }
                    if (!mOrganizationJsonObject.isNull("orgDescription")) {
                        mOrganisationDetail = mOrganizationJsonObject.getString("orgDescription")
                    }
                    if (!mOrganizationJsonObject.isNull("orgImage")) {
                        mOrganisationImage = mOrganizationJsonObject.getString("orgImage")
                    }
                }
                //  Get Opportunity  Details
                if (mType == "6") {
                    mOpportunityJsonObject = mJsonObject.getJSONObject("opportunityDetails")
                }
                if (mType == "12") {
                    mOpportunityJsonObject = mJsonObject.getJSONObject("opportunityDetails")
                }
                if (mType == "13") {
                    mOpportunityJsonObject = mJsonObject.getJSONObject("opportunityDetails")
                }
                if (mType == "14") {
                    mOpportunityJsonObject = mJsonObject.getJSONObject("opportunityDetails")
                }
            } catch (e: Exception) {
                Log.e(TAG, "onMessageReceived: $e")
            }
        }
        mTitle = p0.data.get("title");
        message = p0.data.get("body");
        sendNotification(mTitle, message)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun sendNotification(mTitle: String?, message: String?) {
        var intent: Intent? = null
        var pendingIntent: PendingIntent? = null
        if (mType == "2") {
            Log.i("NotificationType", mType)
            intent = Intent(this, SplashActivity::class.java)
            intent.putExtra("Push", "push")
            intent.putExtra("mType", mType)
            intent.putExtra("isApprove", mIsApprove)
            intent.putExtra("orgTitle", mOrganisationTitle)
            intent.putExtra("orgDetail", mOrganisationDetail)
            intent.putExtra("orgImage", mOrganisationImage)
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        }
        if (mType == "4") {
            intent = Intent(this, SplashActivity::class.java)
            intent.putExtra("Push", "push")
            intent.putExtra("mType", mType)
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        }
        if (mType == "5") {
            intent = Intent(this, SplashActivity::class.java)
            intent.putExtra("Push", "push")
            intent.putExtra("mType", mType)
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        }
        if (mType == "6") {
            intent = Intent(this, SplashActivity::class.java)
            intent.putExtra("Push", "push")
            intent.putExtra("mType", mType)
            intent.putExtra("mOpDetailsJsonObject", mOpportunityJsonObject.toString())
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        }

        if (mType == "12") {
            intent = Intent(this, SplashActivity::class.java)
            intent.putExtra("Push", "push")
            intent.putExtra("mType", mType)
            intent.putExtra("mOpDetailsJsonObject", mOpportunityJsonObject.toString())
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        }

        if (mType == "13") {
            intent = Intent(this, SplashActivity::class.java)
            intent.putExtra("Push", "push")
            intent.putExtra("mType", mType)
            intent.putExtra("mOpDetailsJsonObject", mOpportunityJsonObject.toString())
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        }

        if (mType == "14") {
            intent = Intent(this, SplashActivity::class.java)
            intent.putExtra("Push", "push")
            intent.putExtra("mType", mType)
            intent.putExtra("mOpDetailsJsonObject", mOpportunityJsonObject.toString())
            pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)
        }


        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.app_img_xxhdpi)
                .setColor(getResources().getColor(R.color.green))
                .setContentTitle(mTitle)
                .setContentText(this.message)
                .setAutoCancel(true)
                .setPriority(Notification.PRIORITY_HIGH)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                val channel = NotificationChannel(
                    channelId, getString(R.string.default_notification_channel_name),
                    NotificationManager.IMPORTANCE_HIGH
                )
                channel.enableVibration(true)
                notificationManager.createNotificationChannel(channel)

            }
        }
        if (notificationManager != null) {
            notificationManager.notify(
                NOTIFICATION_ID,
                notificationBuilder.build()
            )
        }
    }

}