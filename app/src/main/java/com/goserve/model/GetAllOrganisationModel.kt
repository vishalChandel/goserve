package com.goserve.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAllOrganisationModel(

	@field:SerializedName("data")
	val data: List<OrganisationDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class OrganisationDataItem(

	@field:SerializedName("orgTitle")
	val orgTitle: String? = null,

	@field:SerializedName("orgID")
	val orgID: String? = null
) : Parcelable
