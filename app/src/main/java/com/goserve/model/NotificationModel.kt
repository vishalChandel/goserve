package com.goserve.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationModel(

	@field:SerializedName("lastPage")
	val lastPage: Boolean? = null,

	@field:SerializedName("data")
	val data: List<NotificationDataItem?>? = null,

	@field:SerializedName("count")
	val count: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
): Parcelable

@Parcelize
data class NotificationDataItem(

	@field:SerializedName("notification_type")
	val notificationType: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("notification_id")
	val notificationId: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("roleType")
	val roleType: String? = null,

	@field:SerializedName("userName")
	val userName: String? = null,

	@field:SerializedName("userID")
	val userID: String? = null,

	@field:SerializedName("isApprove")
	val isApprove: String? = null,

	@field:SerializedName("notification_read_status")
	val notificationReadStatus: String? = null,

	@field:SerializedName("detailsID")
	val detailsID: String? = null,

	@field:SerializedName("organizationDetails")
	val organizationDetails: NotificationOrganizationDetails? = null,

	@field:SerializedName("opportunityDetails")
	val opportunityDetails:  OpportunitiesDataItem? = null,

	@field:SerializedName("otherUserID")
	val otherUserID: String? = null
): Parcelable

@Parcelize
data class NotificationOrganizationDetails(

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("orgDescription")
	val orgDescription: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("orgTitle")
	val orgTitle: String? = null,

	@field:SerializedName("orgAddress")
	val orgAddress: String? = null,

	@field:SerializedName("userName")
	val userName: String? = null,

	@field:SerializedName("orgID")
	val orgID: String? = null,

	@field:SerializedName("isApprove")
	val isApprove: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("orgImage")
	val orgImage: String? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("email")
	val email: String? = null
): Parcelable

@Parcelize
data class OpDetailsDataItem(

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("log")
	val log: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("opID")
	val opID: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("opAddress")
	val opAddress: String? = null,

	@field:SerializedName("orgID")
	val orgID: String? = null,

	@field:SerializedName("opImage")
	val opImage: String? = null,

	@field:SerializedName("isDisable")
	val isDisable: String? = null,

	@field:SerializedName("isStatus")
	val isStatus: String? = null,

	@field:SerializedName("opHour")
	val opHour: String? = null,

	@field:SerializedName("opCreatedBy")
	val opCreatedBy: String? = null,

	@field:SerializedName("startTime")
	val startTime: String? = null,

	@field:SerializedName("endTime")
	val endTime: String? = null,

	@field:SerializedName("peopleRequired")
	val peopleRequired: String? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("startDateInDate")
	val startDateInDate: String? = null,

	@field:SerializedName("endDateInDate")
	val endDateInDate: String? = null,

	@field:SerializedName("dayDiff")
	val dayDiff: String? = null,


	@field:SerializedName("startTimeForAddEvent")
	val startTimeForAddEvent: String? = null,

	@field:SerializedName("endTimeForAddEvent")
	val endTimeForAddEvent: String? = null,

	) : Parcelable

