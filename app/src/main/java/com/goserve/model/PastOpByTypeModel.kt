package com.goserve.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PastOpByTypeModel(

	@field:SerializedName("lastPage")
	val lastPage: Boolean? = null,

	@field:SerializedName("data")
	val data: List<PastOpDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class PastOpDataItem(

	@field:SerializedName("startDateInDate")
	val startDateInDate: String? = null,

	@field:SerializedName("endDate")
	val endDate: String? = null,

	@field:SerializedName("log")
	val log: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("isStatus")
	val isStatus: String? = null,

	@field:SerializedName("opID")
	val opID: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("opAddress")
	val opAddress: String? = null,

	@field:SerializedName("orgID")
	val orgID: String? = null,

	@field:SerializedName("opImage")
	val opImage: String? = null,

	@field:SerializedName("isDisable")
	val isDisable: String? = null,

	@field:SerializedName("opHour")
	val opHour: String? = null,

	@field:SerializedName("endDateInDate")
	val endDateInDate: String? = null,

	@field:SerializedName("opCreatedBy")
	val opCreatedBy: String? = null,

	@field:SerializedName("startTime")
	val startTime: String? = null,

	@field:SerializedName("endTime")
	val endTime: String? = null,

	@field:SerializedName("peopleRequired")
	val peopleRequired: String? = null,

	@field:SerializedName("startDate")
	val startDate: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("dayDiff")
	val dayDiff: String? = null
) : Parcelable
