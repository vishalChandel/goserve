package com.goserve.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ReportReasonModel(

	@field:SerializedName("data")
	val data: List<ReportReasonDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class ReportReasonDataItem(

	@field:SerializedName("reasonId")
	val reasonId: String? = null,

	@field:SerializedName("reportReasons")
	val reportReasons: String? = null
) : Parcelable
