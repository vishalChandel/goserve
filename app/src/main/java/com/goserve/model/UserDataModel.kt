package com.goserve.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserDataModel(

	@field:SerializedName("data")
	val data: UserData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class UserData(

	@field:SerializedName("lastName")
	val lastName: String? = null,

	@field:SerializedName("isRequest")
	val isRequest: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("authToken")
	val authToken: String? = null,

	@field:SerializedName("verified")
	val verified: String? = null,

	@field:SerializedName("appleToken")
	val appleToken: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("userName")
	val userName: String? = null,

	@field:SerializedName("userID")
	val userID: String? = null,

	@field:SerializedName("orgID")
	val orgID: String? = null,

	@field:SerializedName("verificationCode")
	val verificationCode: String? = null,

	@field:SerializedName("googleToken")
	val googleToken: String? = null,

	@field:SerializedName("firstName")
	val firstName: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("hourTracker")
	val hourTracker: String? = null,

	@field:SerializedName("dob")
	val dob: String? = null,

	@field:SerializedName("requestOrgID")
	val requestOrgID: String? = null,

	@field:SerializedName("userRole")
	val userRole: String? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
