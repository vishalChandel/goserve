package com.playerpollmyapp.retrofit

import com.goserve.model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @FormUrlEncoded
    @POST("signUp")
    fun signupRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<UserDataModel>

    @FormUrlEncoded
    @POST("logIn")
    fun signinRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<UserDataModel>

    @FormUrlEncoded
    @POST("forgetPassword")
    fun forgotPwdRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @FormUrlEncoded
    @POST("logOut")
    fun logoutRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @FormUrlEncoded
    @POST("googleLogin")
    fun googleSigninRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<UserDataModel>

    @FormUrlEncoded
    @POST("appleLogin")
    fun appleSigninRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<UserDataModel>

    @FormUrlEncoded
    @POST("checkGoogleAppleTokenExistsByType")
    fun checkGoogleAppleTokenRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<UserDataModel>

    @FormUrlEncoded
    @POST("getAllOrganizationForStudent")
    fun getAllOrganisationForStudentRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<GetAllOrganisationModel>

    @FormUrlEncoded
    @POST("changePassword")
    fun changePwdRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @FormUrlEncoded
    @POST("getProfile")
    fun getProfileRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<GetProfileModel>

    @Multipart
    @POST("editProfile")
    fun editProfileRequest(
        @Part profileImage: MultipartBody.Part?,
        @Part("authToken") userName: RequestBody?,
        @Part("userName") email: RequestBody?,
        @Part("firstName") password: RequestBody?,
        @Part("lastName") countryCode: RequestBody?,
        @Part("orgID") deviceToken: RequestBody?,
        @Part("dob") deviceType: RequestBody?
    ): Call<UserDataModel>

    @FormUrlEncoded
    @POST("getAllOpportunitiesByTypev2")
    fun getAllOpportunitiesByTypeRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<GetAllOpportunitiesByTypeModel>

    @FormUrlEncoded
    @POST("deleteProfileImage")
    fun deleteProfileImageRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @FormUrlEncoded
    @POST("joinOpportunities")
    fun joinOpportunitiesRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @FormUrlEncoded
    @POST("filterOpportunities")
    fun filterOpportunitiesRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<GetAllOpportunitiesByTypeModel>

    @FormUrlEncoded
    @POST("notificationActivity")
    fun notificationRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<NotificationModel>

    @FormUrlEncoded
    @POST("getAllComingPastOpportunitiesByType")
    fun pastOpRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<GetAllOpportunitiesByTypeModel>

    @FormUrlEncoded
    @POST("getAllReportReasons")
    fun reportReasonsRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<ReportReasonModel>

    @FormUrlEncoded
    @POST("addReport")
    fun addReportRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>

    @FormUrlEncoded
    @POST("contactUs")
    fun contactusRequest(
        @FieldMap mParams: Map<String?, String?>?
    ): Call<StatusMsgModel>
}