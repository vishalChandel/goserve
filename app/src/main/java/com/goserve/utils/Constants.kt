package com.goserve.utils

// - - Base Server Url
//const val BASE_URL = "http://www.goserveapp.com/goServeLatest/Api/"

// Staging Url
const val BASE_URL="http://161.97.132.85/goServeLatest/Api/"


//Package Name
const val PACKAGE_NAME="com.android.chrome"

// - - Link Constants
const val LINK_TYPE = "link_type"
const val LINK_ABOUT = "link_about"
const val LINK_PP = "link_pp"
const val LINK_TERMS = "link_terms"

// - - Link Urls
const val ABOUT_WEB_LINK = "http://www.goserveapp.com/goServeLatest/aboutUs.html"
const val PP_WEB_LINK = "http://www.goserveapp.com/goServeLatest/privacyPolicy.html"
const val TERMS_WEB_LINK = "http://www.goserveapp.com/goServeLatest/TermsAndConditions.html"

// - - Constant Values
const val SPLASH_TIME_OUT = 3000
const val DEVICE_TYPE = "2"
const val GOOGLE_SIGN_IN = 123456
const val RESULT_CODE = "result_code"

// - - Shared Preference Keys
const val IS_LOGIN = "is_login"
const val EMAIL = "email"
const val FIRST_NAME= "first_name"
const val LAST_NAME= "last_name"
const val USERNAME = "userName"
const val DOB = "dob"
const val AUTH_TOKEN = "auth_token"
const val USERID = "user_id"
const val ORG_ID = "org_id"
const val PROFILE_IMAGE = "image"
const val RM_EMAIL_USERNAME = "re_email_username"
const val RM_PASSWORD = "re_password"
const val IS_REMEMBER_ME = "is_remember_me"
const val HOUR_TRACKER = "hour_tracker"
const val ORG_TITLE = "org_title"
const val ORG_IMAGE = "org_image"
const val ORG_DETAIL = "org_detail"

// - - Filter Shared Preference Keys
const val START_DATE= "start_date"
const val END_DATE="end_date"
const val FILTER=""
const val SELECT_DAY="select_day"

// - - Search Shared Preference Keys
const val SEARCH_TEXT= ""

// - - Apple SignIn Keys
val CLIENT_ID = "app.goserve.com"
val REDIRECT_URI = "https://goserve-3187a.firebaseapp.com/__/auth/handler"
val SCOPE = "name%20email"
val AUTHURL = "https://appleid.apple.com/auth/authorize"
val TOKENURL = "https://appleid.apple.com/auth/token"

